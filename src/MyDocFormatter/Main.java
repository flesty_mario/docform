package MyDocFormatter;

import java.util.*;

import MyDocFormatter.Database.JDBC;
import MyDocFormatter.UI.*;
import MyDocFormatter.utils.Common;
import MyDocFormatter.utils.PrintedForms.Document;
import MyDocFormatter.utils.PrintedForms.DocumentFileSettingsMaster;
import MyDocFormatter.utils.PrintedForms.XmlMaster;
import org.w3c.dom.Node;

/**
 * Created by crystall on 23.12.2017.
 */
public class Main {

    public static void main(String args[]) {
        System.out.println("MyDocFormatter:: started at " + new Date().toString());

        new Application();
        Common.clearTemporaryCatalog();
    }
}