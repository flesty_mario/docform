package MyDocFormatter.utils;

import MyDocFormatter.Database.DocumentInstance;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by crystall on 03.03.2018.
 */
public class Common {

    public final static ArrayList<String> StringArrayToArrayList(String[] array){
        ArrayList<String> result = new ArrayList<>();
        for (String item: array){
            result.add(item);
        }
        return result;
    }

    public final static void clearTemporaryCatalog(){
        try {
            File tmpDir = new File("./temp");
            if (tmpDir.listFiles() == null){
                return;
            }
            for (File item: tmpDir.listFiles()){
                item.delete();
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
