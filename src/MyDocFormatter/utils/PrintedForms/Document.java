//класс-обертка для работы с .docx файлами: чтение, редактирование и т.д.

package MyDocFormatter.utils.PrintedForms;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import MyDocFormatter.Application.DynamicSystemSettings;
import MyDocFormatter.Database.DocumentInstance;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by crystall on 04.01.2018.
 */
public class Document {
    private String name;                                   //название документа
    private String path;                                   //путь к документу
    private int type;                                      //тип документа
    private String createDateTime;                         //дата время создания
    private String updateDateTime;                         //дата время последнего сохранения
    private int id;                                        //id документа

    private String templateName;
    private ArrayList<org.w3c.dom.Document> documentList;  //распарсенные xml-шаблоны
    private ArrayList<ArrayList<EditableNode>> docXmlEditableNodesList;   //редактируемые узлы из шаблонов (значения, которые необходимо изменить)
    private ArrayList<String> temporaryXmlList;

    private static String className = "Document";
    private static int templateXmlNumber;


    //конструктор при генерации документа
    public Document(String _name, String pathToSave, String _templateName, ArrayList<String> _templateList, ArrayList<String> _xmlList, ArrayList<String> _temporaryXmlList, int _type){
        String methodName = className + ".Creating";
        name = _name;
        type = _type;
        path = pathToSave;
        id = -1;
        templateName = _templateName;
        documentList = new ArrayList<>();
        docXmlEditableNodesList = new ArrayList<>();
        temporaryXmlList = _temporaryXmlList;
        DocumentFileSettingsMaster.setFilesNamesFromSettings(_name, path, _templateName, _xmlList, _temporaryXmlList);
        System.out.println(methodName + ": Building Document " + name);
        System.out.println(methodName + ": Extracting xml configuration file");
        if (DocumentFileSettingsMaster.unpackDocumentFile(_templateName)){
            for (String templateXml: _templateList) {
                System.out.println(methodName + ": Parsing " + templateXml + " xml configuration file");
                documentList.add(getXmlDoc(templateXml));
                if ( documentList.get(documentList.size() - 1) != null) {
                    //System.out.println(methodName + ": Deleting temporary xml file");
                    //DocumentFileSettingsMaster.deleteTemporaryXmlFile();
                    System.out.println(methodName + ": Getting editable nodes from xml");
                    docXmlEditableNodesList.add(XmlMaster.getEditableNodesFromXml(documentList.get(documentList.size() - 1)));
                    System.out.println(methodName + ": Template " + templateXml + " for document " + name + " has been parsed successfully!");
                } else {
                    System.out.println(methodName + ": Cannot parse " + templateXml + "xml configuration file");
                }
            }
            Validation.setStandardDocumentList(new ArrayList<>(documentList));
            createDateTime = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date());
        }
        else{
            documentList = null;
            docXmlEditableNodesList = null;
            System.out.println(methodName + ": Building document " + name + ": cannot extract xml configuration files");
        }
    }

    //конструктор при открытии документа
    public Document(String _name, String pathToSave, String _templateName, ArrayList<String> _xmlList, ArrayList<String> _temporaryXmlList, int _type){
        String methodName = className + ".Opening";
        name = _name;
        path = pathToSave;
        type = _type;
        documentList = new ArrayList<>();
        temporaryXmlList = _temporaryXmlList;
        DocumentFileSettingsMaster.setFilesNamesFromSettings(_name, path, _templateName, _xmlList, _temporaryXmlList);
        System.out.println(methodName + ": Opening Document " + name);
        System.out.println(methodName + ": Extracting xml configuration file");
        if (DocumentFileSettingsMaster.unpackDocumentFile(_templateName)){
            for (int _i = 0; _i < _xmlList.size(); _i++){
                FileInputStream xml = DocumentFileSettingsMaster.fetchEntry(_xmlList.get(_i));
                documentList.add(getXmlDoc(xml));
                //XmlMaster.deleteXmlFile(temporaryXmlList.get(_i));
            }
            //System.out.println(documentList.size());
        }
        else{
            documentList = null;
            docXmlEditableNodesList = null;
            System.out.println(methodName + ": Opening document " + _templateName + ": cannot extract xml configuration files");
        }
    }

    public void setId(int _id){ id = _id; }

    public void setPath(String path){ this.path = path; }

    public void setUpdateDateTime(String _updateDateTime){ updateDateTime = _updateDateTime; }

    public boolean deleteSettingsFile(){
        try {
            File f = new File(path + name);
            f.delete();
            f = new File(path + name.replace(DynamicSystemSettings.docFileExtention, DynamicSystemSettings.fileExtention));
            f.delete();
            return true;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    public org.w3c.dom.Document getParsedXml(int index){
        return documentList.get(index);
    }

    /*public EditableNode getNode(String node){
        for (EditableNode _node: docXmlEditableNodes){
            if (_node.nodeToEdit.equals(node)){
                return _node;
            }
        }
        return null;
    }*/

    /*public ArrayList<EditableNode> getNodeList(){
        return docXmlEditableNodes;
    }*/

    //изменить значение редактируемого узла
    private boolean setNodeValue(String node, String newNodeValue){
        String methodName = className + ".setNodeValue";
        for (EditableNode _node: docXmlEditableNodesList.get(templateXmlNumber)){
            //System.out.println(methodName + ": Setting value for " + _node.nodeToEdit);
            if (_node.nodeToEdit.equals(node)){
                //изменение значения
                _node.newNodeValue = newNodeValue;
                if (XmlMaster.setXmlNodeValue(documentList.get(templateXmlNumber), _node.nodeToEdit, _node.newNodeValue)) {
                    //System.out.println(methodName + ": Setted value " + _node.newNodeValue + " for node: " + _node.nodeToEdit);
                    return true;
                }
                //если в xml-структуре изменить значения не удалось, то откатываем изменения
                else{
                    //System.out.println(methodName + ": Cannot set value " + _node.newNodeValue + " for " + _node.nodeToEdit);
                    _node.newNodeValue = node;
                    //return false;
                }
            }
        }
        return false;
    }

    //подставить значения во все узлы
    public boolean setNodesValues(Map<String, String> _enumeratedValues, int _templateXmlDocument){
        String methodName = className + ".setNodesValues";
        try {
            templateXmlNumber = _templateXmlDocument;
            for (Map.Entry<String, String> entrySet : _enumeratedValues.entrySet()) {
                String key = entrySet.getKey();
                String value = entrySet.getValue();
                setNodeValue(key, value);
            }
            return true;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: " + ex.getMessage());
            return false;
        }
    }

    public boolean validateDocument(){
        return Validation.validateDocument(documentList.get(0));
    }

    public String getName() { return name; }

    public int getId() { return id; }

    public ArrayList<Node> getDocumentContent(){
        ArrayList<Node> content = XmlMaster.getParagraphs(documentList.get(0));
        return content;
    }

    public boolean insertTable(ArrayList<String> columns, ArrayList<ArrayList<String>> mapValue, String tableName, String tablePurpose, int position){
        String methodName = className + ".insertTable";
        try {
            Element headerRight = XmlMaster.createText(documentList.get(0), tableName, "Times New Roman", "28", "right", "", "120", false, false, false);
            Element headerCenter = XmlMaster.createText(documentList.get(0), tablePurpose, "Times New Roman", "28", "center", "80", "120", false, false, false);
            Element table = XmlMaster.createXmlTable(documentList.get(0), columns, mapValue);
            if (table == null || headerCenter == null || headerRight == null){
                throw new Exception("cannot create XML element table");
            }
            XmlMaster.insertElement(documentList.get(0), headerRight, position);
            XmlMaster.insertElement(documentList.get(0), headerCenter, position+1);
            XmlMaster.insertElement(documentList.get(0), table, position+2);
            System.out.println("XML table created successful");
            return true;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public boolean deleteThread(int paragraphIndex, int threadIndex){
        String methodName = className + ".deleteThread";
        try {
            Node paragraph = getDocumentContent().get(paragraphIndex);
            NodeList threadList = paragraph.getChildNodes();
            Node thread = threadList.item(threadIndex);
            return XmlMaster.deleteThread(paragraph, thread);
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public boolean deleteParagraph(int index){
        String methodName = className + ".deleteParagraph";
        try {
            return XmlMaster.deleteParagraph(documentList.get(0), getDocumentContent().get(index));
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public boolean deleteTable(int index){
        String methodName = className + ".deleteTable";
        try {
            return XmlMaster.deleteTable(documentList.get(0), getDocumentContent().get(index));
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public boolean insertList(ArrayList<String> values, int position, boolean bold, boolean italic, boolean underline){
        String methodName = className + ".insertList";
        try {
            ArrayList<Element> valueList = XmlMaster.createXmlList(documentList.get(0), values, bold, italic, underline);
            for (int _i = 0; _i < valueList.size(); _i++) {
                XmlMaster.insertElement(documentList.get(0), valueList.get(_i), position + _i);
            }
            return true;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public boolean insertThread(String value, String font, String size, boolean bold, boolean italic, boolean underline, Node paragraph, int position, boolean isBefore){
        String methodName = className + ".insertThread";
        int pos = (isBefore) ? position : position + 1;
        try {
            XmlMaster.addThread(
                    documentList.get(0),
                    value,
                    font,
                    size,
                    bold,
                    italic,
                    underline,
                    paragraph,
                    pos
            );
            return true;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public boolean insertParagraph(String value, String font, String size, String align, String spacingBefore, String spacingAfter, boolean bold, boolean italic, boolean underline, int position, boolean isBefore){
        String methodName = className + ".insertParagraph";
        int pos = (isBefore) ? position : position + 1;
        try {
            XmlMaster.insertElement(
                    documentList.get(0),
                    XmlMaster.createText(documentList.get(0), value, font, size, align, spacingBefore, spacingAfter, bold, italic, underline),
                    pos
            );
            return true;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    //_parameters = distT, distB, distL, distR, cx, cy, l, t, r, b, id, name, relId, offX, offY
    public boolean insertPicture(String pictureName, String filepath, String[] parameters, int position){
        String methodName = className + ".insertPicture";
        parameters[12] = "rId" + 20;
        try{
            Element pictureFooter = XmlMaster.createXmlImageFooter(documentList.get(0), pictureName);
            Element picture = XmlMaster.createXmlPicture(documentList.get(0), parameters);
            String entryFileName = DocumentFileSettingsMaster.addEntry(filepath, DocumentFileSettingsMaster.EntryType.PICTURE);
            if (!entryFileName.equals("")) {
                Element relationship = XmlMaster.createRelationship(
                        documentList.get(documentList.size() - 1),
                        "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image",
                        parameters[12],
                        entryFileName
                );
                if (picture == null || pictureFooter == null) {
                    throw new Exception("cannot create XML element picture");
                }
                XmlMaster.insertElement(documentList.get(0), picture, position);
                XmlMaster.insertElement(documentList.get(0), pictureFooter, position+1);
                XmlMaster.appendElement(documentList.get(documentList.size() - 1), relationship, "Relationships");
                return true;
            }
            return false;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    //формирование .docx
    public void generateDocument(){
        String methodName = className + ".generateDocument";
        int _i = 0;
        for (org.w3c.dom.Document _document: documentList) {
            if (XmlMaster.createXmlFromDocument(_document, temporaryXmlList.get(_i))) {
                System.out.println(methodName + ": Temporary xml " + temporaryXmlList.get(_i++) + " file created.");
            } else {
                System.out.println(methodName + ": Cannot create temporary xml " + temporaryXmlList.get(_i++) + " file");
            }
        }
        System.out.println(methodName + ": Compressing .docx file");
        if (DocumentFileSettingsMaster.compressFileSettingsToDoc()) {
            System.out.println(methodName + ": Successfully created new document " + name);
        } else {
            System.out.println(methodName + ": An error occurs during creating new document " + name);
        }
        for (org.w3c.dom.Document _document: documentList) {
            XmlMaster.deleteXmlFile(temporaryXmlList.get(documentList.indexOf(_document)));
        }
    }

    public void saveDocument(){
        String methodName = className + ".saveDocument";
        try {
            DocumentFileSettingsMaster.unpackDocumentFile(templateName);
            generateDocument();
            updateDateTime = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date());
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
        }
    }

    public void createDocumentParametersFile(){
        String methodName = className + ".createDocumentParametersFile";
        try {
            String fileName = name.replace(DynamicSystemSettings.docFileExtention, DynamicSystemSettings.fileExtention);
            FileWriter fout = new FileWriter(new File(path + "/" + fileName), true);
            fout.write("" + name);
            fout.write("\r\n");
            fout.write("" + type);
            fout.write("\r\n");
            fout.write("" + updateDateTime);
            fout.write("\r\n");
            fout.write("" + id);
            fout.write("\r\n");
            fout.close();
        }
        catch (FileNotFoundException ex){
            System.out.println(methodName + " FileNotFoundException exception: ");
            ex.printStackTrace();
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
        }
    }

    public DocumentInstance createInstance(){
        DocumentInstance instance = new DocumentInstance();
        instance.name = name;
        instance.id = id;
        instance.type = type;
        instance.updateDateTime = updateDateTime;
        instance.savePath = path;
        return instance;
    }

    //парсинг xml-файла с настройками
    private org.w3c.dom.Document getXmlDoc(String _xmlFileName){
        String methodName = className + ".getXmlDoc";
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            org.w3c.dom.Document parsedXmlDocument = builder.parse(new FileInputStream(new File(_xmlFileName)));
            parsedXmlDocument.getDocumentElement().normalize();
            return parsedXmlDocument;
        }
        catch (ParserConfigurationException ex) {
            System.out.println(methodName + " XML parser configuration exception: " + ex.getMessage());
        }
        catch (SAXException ex){
            System.out.println(methodName + " SAX exception: " + ex.getMessage());
        }
        catch (IOException ex){
            System.out.println(methodName + " input/output exception: " + ex.getMessage());
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: " + ex.getMessage());
        }
        return null;
    }

    private org.w3c.dom.Document getXmlDoc(FileInputStream _xml){
        String methodName = className + ".getXmlDoc";
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            org.w3c.dom.Document parsedXmlDocument = builder.parse(_xml);
            parsedXmlDocument.getDocumentElement().normalize();
            return parsedXmlDocument;
        }
        catch (ParserConfigurationException ex) {
            System.out.println(methodName + " XML parser configuration exception: " + ex.getMessage());
        }
        catch (SAXException ex){
            System.out.println(methodName + " SAX exception: " + ex.getMessage());
        }
        catch (IOException ex){
            System.out.println(methodName + " input/output exception: " + ex.getMessage());
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: " + ex.getMessage());
        }
        return null;
    }
}
