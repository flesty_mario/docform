package MyDocFormatter.utils.PrintedForms;

import java.util.ArrayList;

/**
 * Created by crystall on 06.01.2018.
 */
public class EditableNode{
    public String nodeToEdit;
    public String newNodeValue;
    public ArrayList<String> newNodeValueList;

    public EditableNode(){}

    public EditableNode(String _nodeToEdit, String _newNodeValue){
        this();
        nodeToEdit = _nodeToEdit;
        newNodeValue = _newNodeValue;
        newNodeValueList = new ArrayList<>();
    }
}
