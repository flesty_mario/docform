//статический класс для работы с настройками .docx файла

package MyDocFormatter.utils.PrintedForms;

import MyDocFormatter.Application.DynamicSystemSettings;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;
import com.sun.xml.internal.ws.api.message.saaj.SaajStaxWriter;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Created by crystall on 04.01.2018.
 */
public class DocumentFileSettingsMaster {

    private static class ZipFileEntry{
        public String name;
        public byte[] entry;
        public ZipFileEntry(String _name, byte[] _entry){
            name = _name;
            entry = _entry;
        }
    }

    public enum EntryType{
        PICTURE,
        XML,
        XMLREL
    }

    private static String temporaryDocumentSettingsFilename; //временный xml файл
    private static String templateDocumentFilename; //шаблон, по которому генерируется документ
    private static String documentFilename; //создаваемый по шаблону документ
    private static String documentPath; //каталог куда сохранять

    private static ArrayList<String> documentXmlFilename;
    private static ArrayList<String> xmlDocumentSettingsFilename;
    private static ArrayList<String> temporaryXmlDocumentSettingsFilename;

    private static ArrayList<ZipFileEntry> newEntryList;

    private static String className = "DocumentFileSettingsMaster";

    private static ZipFile zFile;  //файлы .docx открываются как zip-архивы
    private static Enumeration<? extends ZipEntry> zipEntryList; //содержание .docx файлов

    //подготовительные настройки - выбор названия файла и шаблона, по которому потом сгенерируется документ
    public static void setFilesNamesFromSettings(String _documentFilename, String _documentPath, String _templateDocumentFilename, ArrayList<String> _documentXmlList, ArrayList<String> _temporaryXmlFilenameList){
        templateDocumentFilename = _templateDocumentFilename;
        documentFilename = _documentFilename;
        documentPath = _documentPath;
        documentXmlFilename = _documentXmlList;
        temporaryXmlDocumentSettingsFilename = _temporaryXmlFilenameList;
        newEntryList = new ArrayList<>();
    }

    public static boolean unpackDocumentFile(String fileName){
        String methodName = className + ".unpackDocumentFile";
        try {
            File f = new File(fileName);
            zFile = new ZipFile(f);
            zipEntryList = zFile.entries();
            System.out.println(methodName + ": "+ fileName + " unpacked successfully!");
            System.out.println(methodName + ": "+ zFile.size());
            return true;
        }
        catch (IOException ex){
            System.out.println(methodName + " Input/Output exception: " + ex.getMessage());
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: " + ex.getMessage());
        }
        return false;
    }

    //извлечение из .docx файла word/document.xml, который является копией оригинала .docx в формате xml
    public static boolean extractFileSettingsInXml(String documentName, String XmlDocumentName, String tempXmlDocument){
        String methodName = className + ".extractFileSettingsInXml";
        try {
            System.out.println(methodName + ": Unpacking " + documentName);

            if (unpackDocumentFile(documentName)) {

                System.out.println(methodName + ": Extracting " + XmlDocumentName + " from " + documentName);
                ZipEntry zipEntry = zFile.getEntry(XmlDocumentName);
                InputStream is = zFile.getInputStream(zipEntry);

                System.out.println(methodName + ": Forming temporary file " + tempXmlDocument);
                OutputStream fos = new FileOutputStream(tempXmlDocument);
                byte[] buffer = new byte[1024];
                int bytesRead;
                //чтение из входного потока в буфер
                while ((bytesRead = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }
                is.close();
                fos.flush();
                fos.close();
                return true;
            }
            else{
                System.out.println(methodName + ": An error occurs during extracting file settings from " + documentName);
                return false;
            }
        }
        catch (IOException ex){
            System.out.println(methodName + ": Input/Output exception: " + ex.getMessage());
        }
        catch (Exception ex){
            System.out.println(methodName + ": extractFileSettingInXml error: " + ex.getMessage());
        }
        return false;
    }

    public static FileInputStream fetchEntry(String entryName){
        try {
            String fileName = DynamicSystemSettings.tempCatalog +  "/temporary_entry.xml";
            InputStream is = zFile.getInputStream(zFile.getEntry(entryName));
            OutputStream fos = new FileOutputStream(fileName);
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                fos.write(buffer, 0, bytesRead);
            }
            is.close();
            fos.flush();
            fos.close();
            File f = new File(fileName);
            FileInputStream fin = new FileInputStream(f);
            return fin;
        }
        catch (IOException ex){
            System.out.println("fetch entry error: " + ex.getMessage());
            return null;
        }
    }

    public static String addEntry(String filename, EntryType entryType){
        String methodName = className + ".addEntry";
        String entryFileName = "";
        try{
            System.out.println(methodName + ": adding new entry " + filename);
            byte[] buf = Files.readAllBytes(new File(filename).toPath());
            switch (entryType){
                case PICTURE:
                    entryFileName = "image" + (newEntryList.size() + 1) + ".jpeg";
                    newEntryList.add(new ZipFileEntry("word/media/" + entryFileName, buf));
                    break;
            }
            System.out.println(methodName + ": successfuly added entry " + entryFileName + " from file " + filename);
            return "media/" + entryFileName;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return entryFileName;
        }
    }

    //формирование .docx-файла, с добавлением в него xml с новыми значениями
    public static boolean compressFileSettingsToDoc(){
        String methodName = className + ".compressFileSettingsToDoc";
        try{
            ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(documentPath + "" + documentFilename));
            for (Enumeration e = zipEntryList; e.hasMoreElements(); ){
                ZipEntry entryIn = new ZipEntry(((ZipEntry) e.nextElement()).getName());
                int index = documentXmlFilename.indexOf(entryIn.getName());
                System.out.println(methodName + ": packing entry " + entryIn.getName());
                //в документ добавляются все элементы, кроме word/document.xml
                if (index == -1){
                    System.out.println(methodName + ": packing existing entry " +  entryIn.getName());
                    zipOutputStream.putNextEntry(entryIn);
                    InputStream inputStream = zFile.getInputStream(entryIn);
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = inputStream.read(buf)) > 0){
                        zipOutputStream.write(buf, 0, (len < buf.length) ? len: buf.length);
                    }
                }
                //в word/document.xml записываются данные из буфера, полученного из файла document.xml с новыми значениями
                else{
                    System.out.println(methodName + ": packing modified entry " +  temporaryXmlDocumentSettingsFilename.get(index));
                    zipOutputStream.putNextEntry(entryIn);
                    byte[] buf = Files.readAllBytes(new File(temporaryXmlDocumentSettingsFilename.get(index)).toPath());
                    zipOutputStream.write(buf);
                }
                zipOutputStream.closeEntry();
            }
            //добавление новых файлов в архив (если есть)
            for (ZipFileEntry zipEntry: newEntryList){
                zipOutputStream.putNextEntry(new ZipEntry(zipEntry.name));
                zipOutputStream.write(zipEntry.entry);
                zipOutputStream.closeEntry();
            }
            zipOutputStream.close();
            zFile.close();
            return true;
        }
        catch (FileNotFoundException ex){
            System.out.println(methodName + " file not found exception: " + ex.getMessage());
        }
        catch (Exception ex){
            System.out.println(methodName + " error: " + ex.getMessage());
        }
        return false;
    }

    public static boolean deleteTemporaryXmlFile(){
        File xmlFile = new File(temporaryDocumentSettingsFilename);
        if (xmlFile.exists()) {
            return xmlFile.delete();
        }
        return false;
    }
}
