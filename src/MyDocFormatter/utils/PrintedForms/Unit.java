package MyDocFormatter.utils.PrintedForms;

import org.w3c.dom.Node;

/**
 * Created by crystall on 02.04.2018.
 */
public class Unit {
    public int index;
    public Node element;
    public Unit(){}
    public Unit(int index, Node element){
        this.index = index;
        this.element = element;
    }
}
