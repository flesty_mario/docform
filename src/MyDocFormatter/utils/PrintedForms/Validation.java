package MyDocFormatter.utils.PrintedForms;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

/**
 * Created by crystall on 23.02.2018.
 */
public class Validation {

    private static ArrayList<org.w3c.dom.Document> standardDocumentList; //исходные шаблоны, с которыми сравниваем
    private static String className = "Validation";

    //задать исходные шаблоны
    public static void setStandardDocumentList(ArrayList<org.w3c.dom.Document> _standardDocumentList){
        standardDocumentList = _standardDocumentList;
    }

    public static boolean validateDocument(org.w3c.dom.Document document){
        return
                //размер шрифта
                validateDocumentParameters(document, "w:sz", "w:val")
                &&
                //выравнивание
                validateDocumentParameters(document, "w:jc", "w:val")
                &&
                //отступы
                validateDocumentParameters(document, "w:spacing", "w:val");
    }

    private static boolean validateDocumentParameters(org.w3c.dom.Document document, String node, String attribute){
        String methodName = className + ".validateDocumentParameters";
        boolean isMatched = true;
        try {
            NodeList paragraphList = document.getDocumentElement().getElementsByTagName(node);
            NodeList standardParagraphList = standardDocumentList.get(0).getDocumentElement().getElementsByTagName(node);
            if (paragraphList != null) {
                for (int _i = 0; _i < paragraphList.getLength(); _i++) {
                    Node attr = paragraphList.item(_i).getAttributes().getNamedItem(attribute);
                    if (attr != null && !validateParameter(attr.getNodeValue(), standardParagraphList.item(_i).getAttributes().getNamedItem(attribute).getNodeValue())) {
                        isMatched = false;
                    }
                }
            }
            return isMatched;
        }
        catch (Exception ex){
            System.out.println(methodName + "Node: " + node + ", attribute: " + attribute + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    //сравнение параметров редактируемого документа с исходником
    private static boolean validateParameter(String actualSize, String expectedSize){
        return actualSize.equals(expectedSize);
    }
}
