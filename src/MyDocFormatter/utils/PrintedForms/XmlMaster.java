//статический класс для работы с XML документа: изменение редактируемых значений и т.д.

package MyDocFormatter.utils.PrintedForms;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by crystall on 04.01.2018.
 */
public class XmlMaster {

    private static String className = "XmlMaster";

    //private static

    //получить из xml все узлы, значения в которых нужно поменять
    public static ArrayList<EditableNode> getEditableNodesFromXml(org.w3c.dom.Document parsedXmlDocument){
        String methodName = className + ".getEditableNodesFromXml";
        try {
            ArrayList<EditableNode> nodesToEdit = new ArrayList<>();
            NodeList nodes = parsedXmlDocument.getDocumentElement().getElementsByTagName("w:t");
            for (int _i = 0; _i < nodes.getLength(); _i++) {
                NodeList childNodes = nodes.item(_i).getChildNodes();
                for (int _j = 0; _j < childNodes.getLength(); _j++) {
                    //определяем, что узел редактируемый
                    if (isEditableNode(childNodes.item(_j).getNodeValue())) {
                        String node = childNodes.item(_j).getNodeValue();
                        //создается структура для хранения редактируемых узлов и их значений
                        nodesToEdit.add(new EditableNode(node, node));
                    }
                }
            }
            return nodesToEdit;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: " + ex.getMessage());
        }
        return null;
    }

    //создает xml-файл из org.w3c.dom.Document, в котором проводились изменения данных
    public static boolean createXmlFromDocument(org.w3c.dom.Document documentToWrite, String _fileName){
        String methodName = className + ".createXmlFromDocument";
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(documentToWrite);
            StreamResult result = new StreamResult(new File(_fileName));
            transformer.transform(source, result);
            return true;
        }
        catch (TransformerConfigurationException ex){
            System.out.println(methodName + " transformer configuration exception: " + ex.getMessage());
        }
        catch (TransformerException ex){
            System.out.println(methodName + " transformer exception: " + ex.getMessage());
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: " + ex.getMessage());
        }
        return false;
    }

    //удаление xml-файла
    public static boolean deleteXmlFile(String _fileName){
        File xmlFile = new File(_fileName);
        if (xmlFile.exists()) {
            return xmlFile.delete();
        }
        return false;
    }

    //установить новое значение узлу в структуре xml
    public static boolean setXmlNodeValue(org.w3c.dom.Document parsedXmlDocument, String node, String valueNode){
        String methodName = className + ".setXmlNodeValue";
        boolean changed = false;
        try {
            NodeList nodes = parsedXmlDocument.getDocumentElement().getElementsByTagName("w:t");
            for (int _i = 0; _i < nodes.getLength(); _i++) {
                NodeList childNodes = nodes.item(_i).getChildNodes();
                for (int _j = 0; _j < childNodes.getLength(); _j++) {
                    if (childNodes.item(_j).getNodeValue().equals(node)) {
                        childNodes.item(_j).setNodeValue(valueNode);
                        changed = true;
                    }
                }
            }
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: " + ex.getMessage());
        }
        return changed;
    }

    public static boolean appendElement(org.w3c.dom.Document parsedXmlDocument, Element element, String tag){
        String methodName = className + ".appendElement";
        try{
            //Node node = parsedXmlDocument.getDocumentElement().getLastChild();
            //node.appendChild(element);
            NodeList nodeList = parsedXmlDocument.getElementsByTagName(tag);
            for (int _i = 0; _i < nodeList.getLength(); _i++){
                nodeList.item(_i).appendChild(element);
            }
            return true;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    private static boolean deleteElement(org.w3c.dom.Document parsedXmlDocument, Node element){
        NodeList body = parsedXmlDocument.getDocumentElement().getElementsByTagName("w:body");
        for (int _i = 0; _i < body.getLength(); _i++) {
            body.item(_i).removeChild(element);
            return true;
        }
        return false;
    }

    public static boolean deleteThread(Node paragraph, Node thread){
        String methodName = className + ".deleteThread";
        try {
            System.out.println(methodName + ": num of child before - " + thread.getParentNode().getChildNodes().getLength());
            thread.getParentNode().removeChild(thread);
            System.out.println(methodName + ": num of child after- " + paragraph.getChildNodes().getLength());
            return true;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public static boolean deleteParagraph(org.w3c.dom.Document parsedXmlDocument, Node paragraph){
        String methodName = className + ".deleteParagraph";
        try {
            if (!isParagraph(paragraph)) {
                System.out.println(methodName + ": not a paragraph");
                return false;
            }
            if (deleteElement(parsedXmlDocument, paragraph)) {
                System.out.println(methodName + ": remove successfuly");
                return true;
            }
            return false;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public static boolean deleteTable(org.w3c.dom.Document parsedXmlDocument, Node table){
        String methodName = className + ".deleteTable";
        try {
            if (!isTable(table)) {
                System.out.println(methodName + ": not a table");
                return false;
            }
            if (deleteElement(parsedXmlDocument, table)) {
                System.out.println(methodName + ": remove successfuly");
                return true;
            }
            return false;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public static boolean insertElement(org.w3c.dom.Document parsedXmlDocument, Element element, int position){
        String methodName = className + ".insertElement";
        try{
            Node root = parsedXmlDocument.getDocumentElement();
            NodeList nodeList = parsedXmlDocument.getElementsByTagName("w:p");
            if (position > nodeList.getLength()){
                System.out.println("Inserting position " + position + " is more than elements count " + nodeList.getLength());
                return false;
            }
            nodeList.item(position).getParentNode().insertBefore(element, nodeList.item(position));
            //System.out.println(methodName + " size: " + nodeList.getLength());
            return true;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public static boolean addThread(org.w3c.dom.Document parsedXmlDocument, String value, String font, String size, boolean bold, boolean italic, boolean underline, Node paragraph, int pos){
        String methodName = className + ".addThread";
        try{
            if (paragraph == null){
                System.out.println(methodName + ": paragraph is null!");
                return false;
            }
            NodeList childNodeList = paragraph.getChildNodes();
            Element wrpr = createXmlElementWRPR(
                    parsedXmlDocument,
                    createXmlElementWRFonts(parsedXmlDocument, font),
                    null,
                    createXmlElementWSZ(parsedXmlDocument, size),
                    createXmlElementWSZCS(parsedXmlDocument, size),
                    createXmlElementWLang(parsedXmlDocument, "", "ru-RU"),
                    null,
                    bold,
                    italic,
                    underline
            );
            if (pos < childNodeList.getLength()) {
                paragraph.insertBefore(
                        createXmlElementWR(
                                parsedXmlDocument,
                                "",
                                "00E20E8A",
                                "",
                                "",
                                wrpr,
                                null,
                                createXmlElementWT(parsedXmlDocument, value, ""),
                                null,
                                null
                        ),
                        childNodeList.item(pos)
                );
                System.out.println(methodName + ": inserted at " + pos + " position");
            }
            else {
                paragraph.appendChild(createXmlElementWR(
                        parsedXmlDocument,
                        "",
                        "00E20E8A",
                        "",
                        "",
                        wrpr,
                        null,
                        createXmlElementWT(parsedXmlDocument, value, ""),
                        null,
                        null
                ));
                System.out.println(methodName + ": inserted in tail");
            }
            return true;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return false;
        }
    }

    public static ArrayList<Node> getParagraphs(org.w3c.dom.Document parsedXmlDocument){
        String methodName = className + ".getParagraphs";
        try{
            ArrayList<Node> paragraphsList = new ArrayList<>();
            NodeList body = parsedXmlDocument.getDocumentElement().getElementsByTagName("w:body");
            NodeList paragraphs = body.item(0).getChildNodes();
            for (int _i = 0; _i < paragraphs.getLength(); _i++){
                paragraphsList.add(paragraphs.item(_i));
            }
            return paragraphsList;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Unit> getThreadList(Node paragraph){ return getNodeList(paragraph, "w:r"); }

    public static ArrayList<Unit> getNodeList(Node node, String nodeName){
        String methodName = className + ".getNodeList";
        try{
            ArrayList<Unit> nodeList = new ArrayList<>();
            //ArrayList<Node> nodeList = new ArrayList<>();
            if (!nodeName.equals(node.getNodeName())){
                NodeList childNodeList = node.getChildNodes();
                for (int _i = 0; _i < childNodeList.getLength(); _i++){
                    //ArrayList<Node> retNodeRows = getNodeList(childNodeList.item(_i), nodeName);
                    ArrayList<Unit> retNodeRows = getNodeList(childNodeList.item(_i), nodeName);
                    if (retNodeRows.size() != 0){
                        nodeList.addAll(retNodeRows);
                    }
                }
            }
            else {
                int index = 0;
                for (int _i = 0; _i < node.getParentNode().getChildNodes().getLength(); _i++){
                    if (node.getParentNode().getChildNodes().item(_i).isEqualNode(node)){
                        index = _i;
                    }
                }
                nodeList.add(new Unit(index, node));
            }
            return nodeList;
        }
        catch (NullPointerException ex){
            System.out.println(methodName + " null pointer exception: ");
            ex.printStackTrace();
            return null;
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
            return null;
        }
    }

    public static ArrayList<Unit> getTableRows(Node table){
        return getNodeList(table, "w:tr");
    }

    public static ArrayList<Unit> getRowCols(Node row){
        return getNodeList(row, "w:tc");
    }

    public static ArrayList<Unit> getColContent(Node col){
        ArrayList<Unit> colContent = new ArrayList<>();
        for (Unit paragraph: getNodeList(col, "w:p")){
            colContent.addAll(getNodeList(paragraph.element, "w:r"));
        }
        return colContent;
    }

    public static int getRowContentSize(Node row){
        ArrayList<Unit> cols = getRowCols(row);
        int size = 0;
        for (Unit col: cols){
            size += getColContent(col.element).size();
        }
        return size;
    }

    public static ArrayList<Unit> getRowContent(Node row){
        ArrayList<Unit> rowContent = new ArrayList<>();
        ArrayList<Unit> cols = getRowCols(row);
        for (Unit col: cols){
            rowContent.addAll(getColContent(col.element));
        }
        return rowContent;
    }

    public static int getMaxColsNum(Node table){
        int maxColsNum = 0;
        ArrayList<Unit> rows = getTableRows(table);
        for (Unit row: rows){
            int colsNum = getRowContentSize(row.element);
            if (colsNum > maxColsNum){
                maxColsNum = colsNum;
            }
        }
        return maxColsNum;
    }

    public static boolean isTable(Node node){
        return "w:tbl".equals(node.getNodeName());
    }

    public static boolean isParagraph(Node node){
        return "w:p".equals(node.getNodeName());
    }

    public static String getText(Node node){
        String textValue = "";
        if (!"w:t".equals(node.getNodeName())){
            NodeList nodeList = node.getChildNodes();
            for (int _i = 0; _i < nodeList.getLength(); _i++){
                textValue = getText(nodeList.item(_i));
                if (!"".equals(textValue)) {
                    return textValue;
                }
            }
        }
        else {
            if (node.hasChildNodes()) {
                textValue = node.getChildNodes().item(0).getNodeValue();
            }
        }
        return textValue;
    }

    public static void setValue(org.w3c.dom.Document parsedXmlDocument, Node thread, String textValue){
        if (!"w:t".equals(thread.getNodeName())){
            NodeList nodeList = thread.getChildNodes();
            for (int _i = 0; _i < nodeList.getLength(); _i++){
                setValue(parsedXmlDocument, nodeList.item(_i), textValue);
            }
        }
        else {
            if (thread.hasChildNodes()) {
                thread.getChildNodes().item(0).setNodeValue(textValue);
            }
            else{
                thread.appendChild(parsedXmlDocument.createTextNode(textValue));
            }
        }
    }

    public static void addValue(org.w3c.dom.Document parsedXmlDocument, Node thread, String textValue){
        Node wt = createXmlElementWT(parsedXmlDocument, textValue, "");
        thread.appendChild(wt);
    }

    /*public static boolean insertXmlNode(org.w3c.dom.Document parsedXmlDocument, String _nodeAfterInsert, String _nodeValue){
        try {
            Element root = parsedXmlDocument.getDocumentElement();
            root.insertBefore(
                    createXmlNode(parsedXmlDocument, _nodeValue),
                    getXmlNode(parsedXmlDocument, _nodeAfterInsert).getParentNode()
            );
            return true;
        }
        catch (NullPointerException ex){
            System.out.println("insertXmlNode NullPointerException: " + ex.getMessage());
        }
        catch (Exception ex){
            System.out.println("insertXmlNode exception: " + ex.getMessage());
        }
        return false;
    }*/

    private static Node getXmlNode(org.w3c.dom.Document parsedXmlDocument, String _node){
        NodeList nodes = parsedXmlDocument.getDocumentElement().getElementsByTagName("w:t");
        for (int _i = 0; _i < nodes.getLength(); _i++) {
            NodeList childNodes = nodes.item(_i).getChildNodes();
            for (int _j = 0; _j < childNodes.getLength(); _j++) {
                if (childNodes.item(_j).getNodeValue().equals(_node)) {
                    return childNodes.item(_j);
                }
            }
        }
        return null;
    }

    public static String getAttribute(Node node, String attributeName){
        return node.getAttributes().getNamedItem(attributeName).getNodeValue();
    }

    public static ArrayList<Element> createXmlList(org.w3c.dom.Document parsedXmlDocument, ArrayList<String> values, boolean bold, boolean italic, boolean underline){
        try{
            Element wrPr = createXmlElementWRPR(
                    parsedXmlDocument,
                    createXmlElementWRFonts(parsedXmlDocument, "Times New Roman"),
                    null,
                    createXmlElementWSZ(parsedXmlDocument, "28"),
                    null,
                    null,
                    null,
                    bold,
                    italic,
                    underline
            );
            ArrayList<Element> list = new ArrayList<>();
            for (int _i = 0; _i < values.size(); _i++){
                list.add(createXmlElementWP(
                        parsedXmlDocument,
                        "00DF5B1A",
                        "00DF5B1A",
                        "00DF5B1A",
                        "00DF5B1A",
                        createXmlElementWPPR(
                                parsedXmlDocument,
                                createXmlElementWInd(parsedXmlDocument, "708"),
                                createXmlElementWJC(parsedXmlDocument,"both"),
                                null,
                                wrPr
                        ),
                        createXmlElementWR(
                                parsedXmlDocument,
                                "",
                                "",
                                "",
                                "",
                                wrPr,
                                null,
                                createXmlElementWT(
                                        parsedXmlDocument,
                                        (_i + 1) + ". " + values.get(_i) + ((_i == values.size() - 1) ? "." : ";"),
                                        ""
                                ),
                                null,
                                null
                        )
                ));
            }
            return list;
        }
        catch (Exception ex){
            return null;
        }
    }

    public static Element createXmlTable(org.w3c.dom.Document parsedXmlDocument, ArrayList<String> columns, ArrayList<ArrayList<String>> values){
        String[] settings = {"10632","-200","dxa","fixed"};
        try {
            Element tableItem = createXmlElementWTbl(
                    parsedXmlDocument,
                    settings,
                    columns,
                    values
            );
            return tableItem;
        }
        catch (Exception ex){
            return null;
        }
    }

    public static Element createXmlPicture(org.w3c.dom.Document parsedXmlDocument, String[] parameters){
        try{
            Element wrPr = createXmlElementWRPR(
                    parsedXmlDocument,
                    createXmlElementWRFonts(parsedXmlDocument, "Times New Roman"),
                    null,
                    createXmlElementWSZ(parsedXmlDocument, "28"),
                    createXmlElementWSZCS(parsedXmlDocument, "28"),
                    null,
                    null,
                    false,
                    false,
                    false
            );
            Element wrPrExt = createXmlElementWRPR(
                    parsedXmlDocument,
                    createXmlElementWRFonts(parsedXmlDocument, "Times New Roman"),
                    parsedXmlDocument.createElement("w:noProof"),
                    createXmlElementWSZCS(parsedXmlDocument, "28"),
                    createXmlElementWSZCS(parsedXmlDocument, "28"),
                    createXmlElementWLang(parsedXmlDocument, "", "ru-RU"),
                    null,
                    false,
                    false,
                    false
            );
            Element wDrawing = parsedXmlDocument.createElement("w:drawing");
            wDrawing.appendChild(createXmlElementWpInline(
                    parsedXmlDocument,
                    parameters
            ));
            Element wr = createXmlElementWR(
                    parsedXmlDocument,
                    "",
                    "",
                    "",
                    "",
                    wrPrExt,
                    null,
                    null,
                    parsedXmlDocument.createElement("w:lastRenderedPageBreak"),
                    wDrawing
            );
            Element pictureItem = createXmlElementWP(
                    parsedXmlDocument,
                    "00DF5B1A",
                    "00DF5B1A",
                    "00DF5B1A",
                    "00DF5B1A",
                    createXmlElementWPPR(parsedXmlDocument, null, null, null, wrPr),
                    wr
            );
            return pictureItem;
        }
        catch (Exception ex){
            return null;
        }
    }

    public static Element createXmlImageFooter(org.w3c.dom.Document parsedXmlDocument, String _value){
        try{
            Element imageText = parsedXmlDocument.createElement("w:p");
            imageText.setAttribute("w:rsidR", "001D3714");
            imageText.setAttribute("w:rsidRPr", "00EB5D1A");
            imageText.setAttribute("w:rsidRDefault", "001D3714");
            imageText.setAttribute("w:rsidP", "001D3714");
            Element wrpr = createXmlElementWRPR(
                    parsedXmlDocument,
                    createXmlElementWRFonts(
                            parsedXmlDocument,
                            "Times New Roman"
                    ),
                    null,
                    createXmlElementWSZ(parsedXmlDocument, "20"),
                    createXmlElementWSZCS(parsedXmlDocument, "20"),
                    null,
                    null,
                    false,
                    false,
                    false
            );
            Element wpPr = createXmlElementWPPR(
                    parsedXmlDocument,
                    null,
                    createXmlElementWJC(parsedXmlDocument, "center"),
                    null,
                    wrpr
            );
            Element wr = createXmlElementWR(
                    parsedXmlDocument,
                    "",
                    "",
                    "",
                    "",
                    wrpr,
                    null,
                    createXmlElementWT(parsedXmlDocument, _value, ""),
                    null,
                    null
            );
            imageText.appendChild(wpPr);
            imageText.appendChild(wr);
            return imageText;
        }
        catch (Exception ex){
            System.out.println("Error createXMlText: ");
            ex.printStackTrace();
            return null;
        }
    }

    //<Relationship Id="rId9" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="media/image1.jpeg"/>
    public static Element createRelationship(org.w3c.dom.Document parsedXmlDocument, String _type, String _id, String _target){
        try{
            Element rel = parsedXmlDocument.createElement("Relationship");
            if (!_id.equals(""))  rel.setAttribute("Id", _id);
            if (!_type.equals(""))  rel.setAttribute("Type", _type);
            if (!_target.equals(""))  rel.setAttribute("Target", _target);
            return rel;

        }
        catch (Exception ex){
            System.out.println("creating wt: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWT(org.w3c.dom.Document parsedXmlDocument, String _value, String _space){
        try{
            Element wt = parsedXmlDocument.createElement("w:t");
            if (!_space.equals(""))  wt.setAttribute("xml:space", _space);
            wt.appendChild(parsedXmlDocument.createTextNode(_value));
            return wt;

        }
        catch (Exception ex){
            System.out.println("creating wt: " + ex.getMessage());
            return null;
        }
    }

    public static Element createText(org.w3c.dom.Document parsedXmlDocument, String value, String font, String size, String align, String spacingBefore, String spacingAfter, boolean bold, boolean italic, boolean underline){
        try{
            Element wrpr = createXmlElementWRPR(
                    parsedXmlDocument,
                    createXmlElementWRFonts(parsedXmlDocument, font),
                    null,
                    createXmlElementWSZ(parsedXmlDocument, size),
                    createXmlElementWSZCS(parsedXmlDocument, size),
                    createXmlElementWLang(parsedXmlDocument, "", "ru-RU"),
                    null,
                    bold,
                    italic,
                    underline
            );
            Element text = createXmlElementWP(
                    parsedXmlDocument,
                    "00442D3C",
                    "00E20E8A",
                    "00442D3C",
                    "00442D3C",
                    createXmlElementWPPR(
                            parsedXmlDocument,
                            createXmlElementWSpacing(parsedXmlDocument, spacingAfter, spacingBefore),
                            createXmlElementWJC(parsedXmlDocument, align),
                            createXmlElementWInd(parsedXmlDocument, "500"),
                            wrpr
                    ),
                    createXmlElementWR(
                            parsedXmlDocument,
                            "",
                            "00E20E8A",
                            "",
                            "",
                            wrpr,
                            null,
                            createXmlElementWT(parsedXmlDocument, value, ""),
                            null,
                            null
                    )
            );
            return text;
        }
        catch (Exception ex){
            System.out.println("Error creating text: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementWP(org.w3c.dom.Document parsedXmlDocument, String _rsidR, String _rsidRPr, String _rsidRDefault, String _rsidP, Element _wpPr, Element _wr){
        try {
            Element wp = parsedXmlDocument.createElement("w:p");
            if (!_rsidR.equals(""))          wp.setAttribute("w:rsidR", _rsidR);
            if (!_rsidRPr.equals(""))        wp.setAttribute("w:rsidRPr", _rsidRPr);
            if (!_rsidRDefault.equals(""))   wp.setAttribute("w:rsidRDefault", _rsidRDefault);
            if (!_rsidP.equals(""))          wp.setAttribute("w:rsidP", _rsidP);
            if (_wpPr != null)               wp.appendChild(_wpPr);
            if (_wr != null)                 wp.appendChild(_wr);
            return wp;
        }
        catch (Exception ex){
            System.out.println("creating wp: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWR(org.w3c.dom.Document parsedXmlDocument, String wrsidR, String wrsidRPr, String wrsidRDefault, String wrsidP, Element wrPr, Element wtab, Element wt, Element wlastRenderedPageBreak, Element wdrawing){
        try{
            Element wr = parsedXmlDocument.createElement("w:r");
            if (!wrsidR.equals(""))                             wr.setAttribute("w:rsidR", wrsidR);
            if (!wrsidRPr.equals(""))                           wr.setAttribute("w:rsidRPr", wrsidRPr);
            if (!wrsidRDefault.equals(""))                      wr.setAttribute("w:rsidRDefault", wrsidRDefault);
            if (!wrsidP.equals(""))                             wr.setAttribute("w:rsidP", wrsidP);
            if (wrPr != null)                                   wr.appendChild(wrPr);
            if (wtab != null)                                   wr.appendChild(wtab);
            if (wt != null)                                     wr.appendChild(wt);
            if (wlastRenderedPageBreak != null)                 wr.appendChild(wlastRenderedPageBreak);
            if (wdrawing != null)                               wr.appendChild(wdrawing);
            return wr;
        }
        catch (Exception ex){
            System.out.println("creating wr: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWPPR(org.w3c.dom.Document parsedXmlDocument, Element wSpacing, Element wjc, Element wInd, Element wrpr){
        try{
            Element wpPr = parsedXmlDocument.createElement("w:pPr");
            if (wSpacing != null)     wpPr.appendChild(wSpacing);
            if (wjc != null)          wpPr.appendChild(wjc);
            if (wInd != null)         wpPr.appendChild(wInd);
            if (wrpr != null)         wpPr.appendChild(wrpr);
            return wpPr;
        }
        catch (Exception ex){
            System.out.println("creating wpPr: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWJC(org.w3c.dom.Document parsedXmlDocument, String _wval){
        try{
            Element wjc = parsedXmlDocument.createElement("w:jc");
            if (!_wval.equals(""))   wjc.setAttribute("w:val", _wval);
            return wjc;
        }
        catch (Exception ex){
            System.out.println("creating wjc: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWRPR(org.w3c.dom.Document parsedXmlDocument, Element wFonts, Element wnoProof, Element wsz, Element wszCs, Element wlang, Element wSpacing, boolean bold, boolean italic, boolean underline){
        try{
            Element wrpr = parsedXmlDocument.createElement("w:rPr");
            if (wFonts != null)      wrpr.appendChild(wFonts);
            if (wnoProof != null)    wrpr.appendChild(wnoProof);
            if (wsz != null)         wrpr.appendChild(wsz);
            if (wszCs != null)       wrpr.appendChild(wszCs);
            if (wlang != null)       wrpr.appendChild(wlang);
            if (wSpacing != null)    wrpr.appendChild(wSpacing);
            if (bold)    wrpr.appendChild(parsedXmlDocument.createElement("w:b"));
            if (italic)  wrpr.appendChild(parsedXmlDocument.createElement("w:i"));
            if (underline)  {
                Element wu = parsedXmlDocument.createElement("w:b");
                wu.setAttribute("w:val", "Single");
                wrpr.appendChild(wu);
            }
            return wrpr;
        }
        catch (Exception ex){
            System.out.println("creating wrPr: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWLang(org.w3c.dom.Document parsedXmlDocument, String _langval, String _lang){
        try{
            Element wLang = parsedXmlDocument.createElement("w:rPr");
            if (!_langval.equals(""))    wLang.setAttribute("w:val", _langval);
            if (_lang.equals("ru-RU"))   wLang.setAttribute("w:eastAsia", _lang);
            return wLang;
        }
        catch (Exception ex){
            System.out.println("creating wLang: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWRFonts(org.w3c.dom.Document parsedXmlDocument, String _font){
        try{
            Element wrFonts = parsedXmlDocument.createElement("w:rFonts");
            if (!_font.equals("")) {
                wrFonts.setAttribute("w:ascii", _font);
                wrFonts.setAttribute("w:hAnsi", _font);
            }
            return wrFonts;
        }
        catch (Exception ex){
            System.out.println("creating wrFonts: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWSZ(org.w3c.dom.Document parsedXmlDocument, String _size){
        try {
            Element wsz = parsedXmlDocument.createElement("w:sz");
            if (!_size.equals("")) wsz.setAttribute("w:val", _size);
            return wsz;
        }
        catch (Exception ex){
            System.out.println("creating wsz: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWSZCS(org.w3c.dom.Document parsedXmlDocument, String _size){
        try{
            Element wszCs = parsedXmlDocument.createElement("w:szCs");
            if (!_size.equals(""))   wszCs.setAttribute("w:val", _size);
            return wszCs;
        }
        catch (Exception ex){
            System.out.println("creating wszCs: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWInd(org.w3c.dom.Document parsedXmlDocument, String _firstLine){
        try {
            Element wInd = parsedXmlDocument.createElement("w:ind");
            if (!_firstLine.equals(""))    wInd.setAttribute("w:firstLine", _firstLine);
            return wInd;
        }
        catch (Exception ex){
            System.out.println("Error creating wInd: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementWSpacing(org.w3c.dom.Document parsedXmlDocument, String _spacingAfter, String _spacingBefore) {
        try {
            Element WSpacing = parsedXmlDocument.createElement("w:spacing");
            if (!_spacingAfter.equals("")) WSpacing.setAttribute("w:after", _spacingAfter);
            if (!_spacingBefore.equals("")) WSpacing.setAttribute("w:before", _spacingBefore);
            return WSpacing;
        } catch (Exception ex) {
            System.out.println("creating WSpacing: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWTbl(org.w3c.dom.Document parsedXmlDocument, String[] settings, ArrayList<String> gridColList, ArrayList<ArrayList<String>> mapValue){
        try{
            Element wtbl = parsedXmlDocument.createElement("w:tbl");
            wtbl.appendChild(createXmlElementWTblPr(
                    parsedXmlDocument,
                    settings[0],
                    settings[1],
                    settings[2],
                    settings[3],
                    createXmlElementWTblBorders(
                            parsedXmlDocument,
                            "single",
                            "4",
                            "0",
                            "auto"
                    ),
                    createXmlElementWtblLook(
                            parsedXmlDocument,
                            "04A0",
                            "1",
                            "0",
                            "1",
                            "0",
                            "0",
                            "1"
                    )
            ));
            wtbl.appendChild(createXmlElementWTblGrid(
                    parsedXmlDocument,
                    gridColList
            ));
            for (ArrayList<String> values: mapValue){
                wtbl.appendChild(createXmlElementWTR(
                        parsedXmlDocument,
                        "009863AB",
                        "00E20E8A",
                        "00890DE4",
                        gridColList,
                        values
                ));
            }
            return wtbl;
        }
        catch (Exception ex) {
            System.out.println("creating WTbl: " + ex.getMessage());
            return null;
        }

    }

    private static Element createXmlElementWTC(org.w3c.dom.Document parsedXmlDocument, String _width, String _value){
        try{
            Element wtc = parsedXmlDocument.createElement("w:tc");
            Element wrpr = createXmlElementWRPR(
                    parsedXmlDocument,
                    createXmlElementWRFonts(parsedXmlDocument, "Times New Roman"),
                    null,
                    createXmlElementWSZ(parsedXmlDocument, "28"),
                    createXmlElementWSZCS(parsedXmlDocument, "28"),
                    createXmlElementWLang(parsedXmlDocument, "", "ru-RU"),
                    null,
                    false,
                    false,
                    false
            );
            wtc.appendChild(createXmlElementWTCPR(parsedXmlDocument, _width, "dxa", "auto", "auto", "clear"));
            wtc.appendChild(createXmlElementWP(
                    parsedXmlDocument,
                    "005F16BF",
                    "00E20E8A",
                    "004D7731",
                    "00890DE4",
                    createXmlElementWPPR(
                            parsedXmlDocument,
                            createXmlElementWSpacing(parsedXmlDocument, "0", ""),
                            createXmlElementWJC(parsedXmlDocument, "both"),
                            null,
                            wrpr
                    ),
                    createXmlElementWR(
                            parsedXmlDocument,
                            "",
                            "00E20E8A",
                            "",
                            "",
                            wrpr,
                            null,
                            createXmlElementWT(parsedXmlDocument, _value, ""),
                            null,
                            null
                    )
            ));
            return wtc;

        }
        catch (Exception ex){
            System.out.println("creating tc: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWTCPR(org.w3c.dom.Document parsedXmlDocument, String _width, String _type, String _color, String _fill, String _val){
        try{
            Element wtcPr = parsedXmlDocument.createElement("w:tcPr");
            Element wtcW = parsedXmlDocument.createElement("w:tcW");
            if (!_width.equals("")) wtcW.setAttribute("w:w", _width);
            if (!_type.equals(""))  wtcW.setAttribute("w:type", _type);
            Element wshd = parsedXmlDocument.createElement("w:shd");
            if (!_val.equals(""))  wshd.setAttribute("w:val", _val);
            if (!_color.equals(""))  wshd.setAttribute("w:color", _color);
            if (!_fill.equals(""))  wshd.setAttribute("w:fill", _fill);
            wtcPr.appendChild(wtcW);
            wtcPr.appendChild(wshd);
            return wtcPr;

        }
        catch (Exception ex){
            System.out.println("creating tcPr: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWTR(org.w3c.dom.Document parsedXmlDocument, String wrsidR, String wrsidRPr, String wrsidTr, ArrayList<String> gridColList, ArrayList<String> gridColValueList){
        try{
            Element wtr = parsedXmlDocument.createElement("w:tr");
            if (!wrsidR.equals(""))      wtr.setAttribute("w:rsidR", wrsidR);
            if (!wrsidRPr.equals(""))    wtr.setAttribute("w:rsidRPr", wrsidRPr);
            if (!wrsidTr.equals(""))     wtr.setAttribute("w:rsidTr", wrsidTr);
            for (int _i = 0; _i < gridColList.size(); _i++){
                wtr.appendChild(createXmlElementWTC(
                        parsedXmlDocument,
                        gridColList.get(_i),
                        gridColValueList.get(_i)
                ));
            }
            return wtr;

        }
        catch (Exception ex){
            System.out.println("creating tr: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWTblBorders(org.w3c.dom.Document parsedXmlDocument, String type, String size, String space, String color){
        try {
            String[] borders = {"top", "left", "bottom", "right", "insideH", "insideV"};
            Element wtblBorders = parsedXmlDocument.createElement("w:tblBorders");
            for (String border: borders){
                wtblBorders.appendChild(createXmlElementWTblBorder(
                        parsedXmlDocument,
                        border,
                        type,
                        size,
                        space,
                        color
                ));
            }
            return wtblBorders;
        }
        catch (Exception ex){
            System.out.println("creating wtblBorders: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWtblLook(org.w3c.dom.Document parsedXmlDocument, String value, String firstRow, String lastRow, String firstColumn, String lastColumn, String noHBand, String noVBand){
        try {
            Element wtblLook = parsedXmlDocument.createElement("w:tblLook");
            if (!value.equals(""))        wtblLook.setAttribute("w:val", value);
            if (!firstRow.equals(""))     wtblLook.setAttribute("w:firstRow", firstRow);
            if (!lastRow.equals(""))      wtblLook.setAttribute("w:lastRow", lastRow);
            if (!firstColumn.equals(""))  wtblLook.setAttribute("w:firstColumn", firstColumn);
            if (!lastColumn.equals(""))   wtblLook.setAttribute("w:lastColumn", lastColumn);
            if (!noHBand.equals(""))      wtblLook.setAttribute("w:noHBand", noHBand);
            if (!noVBand.equals(""))      wtblLook.setAttribute("w:noVBand", noVBand);
            return wtblLook;
        }
        catch (Exception ex){
            System.out.println("creating wtblLook: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWTblBorder(org.w3c.dom.Document parsedXmlDocument, String align, String type, String size, String space, String color){
        try {
            String element = align.equals("top") ? "w:top" : (
                    align.equals("left") ? "w:left" : (
                            align.equals("bottom") ? "w:bottom" : (
                                    align.equals("right") ? "w:right" : (
                                            align.equals("insideH") ? "w:insideH" : "w:insideV"
                                    ))));
            Element trow = parsedXmlDocument.createElement(element);
            if (!type.equals(""))  trow.setAttribute("w:val", type);
            if (!size.equals(""))  trow.setAttribute("w:sz", size);
            if (!space.equals("")) trow.setAttribute("w:space", space);
            if (!color.equals("")) trow.setAttribute("w:color", color);
            return trow;
        }
        catch (Exception ex){
            System.out.println("creating tblBorder: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWTblGrid(org.w3c.dom.Document parsedXmlDocument, ArrayList<String> gridColList){
        try{
            Element wtblGrid = parsedXmlDocument.createElement("w:tblGrid");
            for (String gridCol: gridColList){
                Element wgridCol = parsedXmlDocument.createElement("w:gridCol");
                wgridCol.setAttribute("w:w", gridCol);
                wtblGrid.appendChild(wgridCol);
            }
            return wtblGrid;
        }
        catch (Exception ex){
            System.out.println("creating wtblGrid: " + ex.getMessage());
            return null;
        }
    }

    private static Element createXmlElementWTblPr(org.w3c.dom.Document parsedXmlDocument, String _widthW, String _widthInd, String _type, String _typeLayout, Element _wtblBorders, Element _tblLook){
        try{
            Element wtblPr = parsedXmlDocument.createElement("w:tblPr");
            Element wtblW = parsedXmlDocument.createElement("w:tblW");
            if (!_widthW.equals(""))  wtblW.setAttribute("w:w", _widthW);
            if (!_type.equals(""))    wtblW.setAttribute("w:type", _type);
            Element wtblInd = parsedXmlDocument.createElement("w:tblInd");
            if (!_widthInd.equals(""))  wtblInd.setAttribute("w:w", _widthInd);
            if (!_type.equals(""))      wtblInd.setAttribute("w:type", _type);
            Element wtblLayout = parsedXmlDocument.createElement("w:tblLayout");
            if (!_typeLayout.equals("")) wtblLayout.setAttribute("w:type", _typeLayout);
            wtblPr.appendChild(wtblW);
            wtblPr.appendChild(wtblInd);
            wtblPr.appendChild(_wtblBorders);
            wtblPr.appendChild(wtblLayout);
            wtblPr.appendChild(_tblLook);
            return wtblPr;

        }
        catch (Exception ex){
            System.out.println("Error creating wtblPr: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementWpExtent(org.w3c.dom.Document parsedXmlDocument, String _cx, String _cy){
        try{
            Element wpExtent = parsedXmlDocument.createElement("wp:extent");
            if (!_cx.equals(""))  wpExtent.setAttribute("cx", _cx);
            if (!_cy.equals(""))  wpExtent.setAttribute("cy", _cy);
            return wpExtent;
        }
        catch (Exception ex){
            System.out.println("Error creating wpExtent: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementWpEffectExtent(org.w3c.dom.Document parsedXmlDocument, String _left, String _top, String _right, String _bottom){
        try{
            Element wpEffectExtent = parsedXmlDocument.createElement("wp:effectExtent");
            if (!_left.equals(""))    wpEffectExtent.setAttribute("l", _left);
            if (!_top.equals(""))     wpEffectExtent.setAttribute("t", _top);
            if (!_right.equals(""))   wpEffectExtent.setAttribute("r", _right);
            if (!_bottom.equals(""))  wpEffectExtent.setAttribute("b", _bottom);
            return wpEffectExtent;
        }
        catch (Exception ex){
            System.out.println("Error creating wpEffectExtent: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementDocPr(org.w3c.dom.Document parsedXmlDocument, String _id, String _name){
        try{
            Element wpDocPr = parsedXmlDocument.createElement("wp:docPr");
            if (!_id.equals(""))      wpDocPr.setAttribute("id", _id);
            if (!_name.equals(""))    wpDocPr.setAttribute("name", _name);
            return wpDocPr;
        }
        catch (Exception ex){
            System.out.println("Error creating wpDocPr: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementAgraphicFrameLocks(org.w3c.dom.Document parsedXmlDocument, String _noChangeAspect){
        try{
            Element agraphicFrameLocks = parsedXmlDocument.createElement("a:graphicFrameLocks");
            agraphicFrameLocks.setAttribute("xmlns:a", "http://schemas.openxmlformats.org/drawingml/2006/main");
            if (!_noChangeAspect.equals(""))    agraphicFrameLocks.setAttribute("noChangeAspect", _noChangeAspect);
            return agraphicFrameLocks;
        }
        catch (Exception ex){
            System.out.println("Error creating agraphicFrameLocks: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementPicnvPicPr(org.w3c.dom.Document parsedXmlDocument, String _id, String _name, String _noChangeAspect, String _noChangeArrowheads){
        try{
            Element PicnvPicPr = parsedXmlDocument.createElement("pic:nvPicPr");

            Element piccNvPr = parsedXmlDocument.createElement("pic:cNvPr");
            if (!_id.equals(""))      piccNvPr.setAttribute("id", _id);
            if (!_name.equals(""))    piccNvPr.setAttribute("name", _name);

            Element apicLocks = parsedXmlDocument.createElement("a:picLocks");
            if (!_noChangeAspect.equals(""))        apicLocks.setAttribute("noChangeAspect", _noChangeAspect);
            if (!_noChangeArrowheads.equals(""))    apicLocks.setAttribute("noChangeArrowheads", _noChangeArrowheads);

            Element piccNvPipPr = parsedXmlDocument.createElement("pic:cNvPicPr");
            piccNvPipPr.appendChild(apicLocks);

            PicnvPicPr.appendChild(piccNvPr);
            PicnvPicPr.appendChild(piccNvPipPr);
            return PicnvPicPr;
        }
        catch (Exception ex){
            System.out.println("Error creating PicnvPicPr: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementPicBlipFill(org.w3c.dom.Document parsedXmlDocument, String _rembed, String _uri, String _a14val){
        try{
            Element PicBlipFill = parsedXmlDocument.createElement("pic:blipFill");
            Element ablip = parsedXmlDocument.createElement("a:blip");
            if (!_rembed.equals(""))   ablip.setAttribute("r:embed", _rembed);

            Element a14 = parsedXmlDocument.createElement("a14:useLocalDpi");
            a14.setAttribute("xmlns:a14", "http://schemas.microsoft.com/office/drawing/2010/main");
            if (!_a14val.equals(""))   a14.setAttribute("val", _a14val);

            Element aext = parsedXmlDocument.createElement("a:ext");
            if (!_uri.equals(""))   aext.setAttribute("uri", _uri);
            aext.appendChild(a14);

            Element aextList = parsedXmlDocument.createElement("a:extList");
            aextList.appendChild(aext);
            ablip.appendChild(aextList);

            Element astretch = parsedXmlDocument.createElement("a:stretch");
            astretch.appendChild(parsedXmlDocument.createElement("a:fillRect"));

            PicBlipFill.appendChild(ablip);
            PicBlipFill.appendChild(parsedXmlDocument.createElement("a:srtRect"));
            PicBlipFill.appendChild(astretch);

            return PicBlipFill;
        }
        catch (Exception ex){
            System.out.println("Error creating PicBlipFill: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementAoff(org.w3c.dom.Document parsedXmlDocument, String _offX, String _offY){
        try{
            Element Aoff = parsedXmlDocument.createElement("a:off");
            if (!_offX.equals(""))   Aoff.setAttribute("x", _offX);
            if (!_offY.equals(""))   Aoff.setAttribute("y", _offY);
            return Aoff;
        }
        catch (Exception ex){
            System.out.println("Error creating Aoff: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementAext(org.w3c.dom.Document parsedXmlDocument, String _cx, String _cy){
        try{
            Element Aext = parsedXmlDocument.createElement("a:off");
            if (!_cx.equals(""))   Aext.setAttribute("cx", _cx);
            if (!_cy.equals(""))   Aext.setAttribute("cy", _cy);
            return Aext;
        }
        catch (Exception ex){
            System.out.println("Error creating Aext: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementPicSpPr(org.w3c.dom.Document parsedXmlDocument, String _bwMode, String _offX, String _offY, String _cx, String _cy){
        try{
            Element PicSpPr = parsedXmlDocument.createElement("pic:spPr");
            if (!_bwMode.equals(""))  PicSpPr.setAttribute("bwMode", _bwMode);
            Element axfrm = parsedXmlDocument.createElement("a:xfrm");
            axfrm.appendChild(createXmlElementAoff(parsedXmlDocument, _offX, _offY));
            axfrm.appendChild(createXmlElementAext(parsedXmlDocument, _cx, _cy));

            Element aprstGeom = parsedXmlDocument.createElement("a:prstGeom");
            aprstGeom.setAttribute("prst", "rect");
            aprstGeom.appendChild(parsedXmlDocument.createElement("a:avLst"));

            Element anoFill = parsedXmlDocument.createElement("a:noFill");

            Element aln = parsedXmlDocument.createElement("a:ln");
            aln.appendChild(parsedXmlDocument.createElement("a:noFill"));

            PicSpPr.appendChild(axfrm);
            PicSpPr.appendChild(aprstGeom);
            PicSpPr.appendChild(anoFill);
            PicSpPr.appendChild(aln);
            return PicSpPr;
        }
        catch (Exception ex){
            System.out.println("Error creating PicSpPr: ");
            ex.printStackTrace();
            return null;
        }
    }

    private static Element createXmlElementPicPic(org.w3c.dom.Document parsedXmlDocument, String _name, String _relId, String _offX, String _offY, String _cx, String _cy){
        try{
            Element PicPic = parsedXmlDocument.createElement("pic:pic");
            PicPic.setAttribute("xmlns:pic","http://schemas.openxmlformats.org/drawingml/2006/picture");
            PicPic.appendChild(createXmlElementPicnvPicPr(
                    parsedXmlDocument,
                    "0",
                    _name,
                    "1",
                    "1")
            );
            PicPic.appendChild(createXmlElementPicBlipFill(
                    parsedXmlDocument,
                    _relId,
                    "{28A0092B-C50C-407E-A947-70E740481C1C}",
                    "0"
            ));
            PicPic.appendChild(createXmlElementPicSpPr(
                    parsedXmlDocument,
                    "auto",
                    _offX,
                    _offY,
                    _cx,
                    _cy
            ));
            return PicPic;
        }
        catch (Exception ex){
            System.out.println("Error creating PicPic: ");
            ex.printStackTrace();
            return null;
        }
    }

    //_parameters = distT, distB, distL, distR, cx, cy, l, t, r, b, id, name, uri, offX, offY
    private static Element createXmlElementWpInline(org.w3c.dom.Document parsedXmlDocument, String[] _parameters){
        try{
            Element wpInline = parsedXmlDocument.createElement("wp:inline");
            wpInline.setAttribute("distT", _parameters[0]);
            wpInline.setAttribute("distB", _parameters[1]);
            wpInline.setAttribute("distL", _parameters[2]);
            wpInline.setAttribute("distR", _parameters[3]);
            wpInline.appendChild(createXmlElementWpExtent(parsedXmlDocument, _parameters[4], _parameters[5]));
            wpInline.appendChild(createXmlElementWpEffectExtent(
                    parsedXmlDocument,
                    _parameters[6],
                    _parameters[7],
                    _parameters[8],
                    _parameters[9]
            ));
            wpInline.appendChild(createXmlElementDocPr(
                    parsedXmlDocument,
                    _parameters[10],
                    _parameters[11]
            ));

            Element cNvGraphicFramePr = parsedXmlDocument.createElement("wp:cNvGraphicFramePr");
            cNvGraphicFramePr.appendChild(createXmlElementAgraphicFrameLocks(parsedXmlDocument, "1"));

            Element aGraphicData = parsedXmlDocument.createElement("a:graphicData");
            aGraphicData.setAttribute("uri", "http://schemas.openxmlformats.org/drawingml/2006/picture");
            aGraphicData.appendChild(createXmlElementPicPic(
                    parsedXmlDocument,
                    _parameters[11],
                    _parameters[12],
                    _parameters[13],
                    _parameters[14],
                    _parameters[4],
                    _parameters[5]
            ));

            Element aGraphic = parsedXmlDocument.createElement("a:graphic");
            aGraphic.setAttribute("xmlns:a", "http://schemas.openxmlformats.org/drawingml/2006/main");
            aGraphic.appendChild(aGraphicData);

            wpInline.appendChild(cNvGraphicFramePr);
            wpInline.appendChild(aGraphic);

            return wpInline;
        }
        catch (Exception ex){
            System.out.println("Error creating WpInline: ");
            ex.printStackTrace();
            return null;
        }
    }

    //проверка, что узел содержит значение, которое нужно заменить (например <NAME1>)
    private static boolean isEditableNode(String _node){
        return _node.contains("<") && _node.contains(">") && (_node.indexOf('>') > _node.indexOf('<'));
    }

}

/*       Список
                <w:p w:rsidR="004A657E" w:rsidRDefault="00DF5B1A">
                    <w:pPr>
                        <w:jc w:val="both"/>
                        <w:rPr>
                            <w:spacing w:val="-6"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r>
                        <w:rPr>
                            <w:spacing w:val="-6"/>
                        </w:rPr>
                        <w:tab/>
                        <w:t>а) <VALUE1>;</w:t>
                    </w:r>
                </w:p>
 */


/*     Картинки
<w:p w:rsidR="001D3714" w:rsidRPr="00E20E8A" w:rsidRDefault="00DD27E1" w:rsidP="001D3714">
            <w:pPr>
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:noProof/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:eastAsia="ru-RU"/>
                </w:rPr>
                <w:lastRenderedPageBreak/>
                <w:drawing>
                    <wp:inline distT="0" distB="0" distL="0" distR="0">
                        <wp:extent cx="5200650" cy="7867650"/>
                        <wp:effectExtent l="0" t="0" r="0" b="0"/>
                        <wp:docPr id="20" name="Рисунок 2"/>
                        <wp:cNvGraphicFramePr>
                            <a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/>
                        </wp:cNvGraphicFramePr>
                            <a:graphic
                                xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                                <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                    <pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                        <pic:nvPicPr>
                                            <pic:cNvPr id="0" name="Рисунок 2"/>
                                            <pic:cNvPicPr>
                                                <a:picLocks noChangeAspect="1" noChangeArrowheads="1"/>
                                            </pic:cNvPicPr>
                                        </pic:nvPicPr>
                                        <pic:blipFill>
                                            <a:blip r:embed="rId9">
                                                <a:extLst>
                                                    <a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                                                        <a14:useLocalDpi
                                                            xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/>
                                                        </a:ext>
                                                    </a:extLst>
                                                </a:blip>
                                                <a:srcRect/>
                                                <a:stretch>
                                                    <a:fillRect/>
                                                </a:stretch>
                                         </pic:blipFill>
                                          <pic:spPr bwMode="auto">
                                                <a:xfrm>
                                                    <a:off x="0" y="0"/>
                                                    <a:ext cx="5200650" cy="7867650"/>
                                                </a:xfrm>
                                                <a:prstGeom prst="rect">
                                                    <a:avLst/>
                                                </a:prstGeom>
                                                <a:noFill/>
                                                <a:ln>
                                                    <a:noFill/>
                                                </a:ln>
                                          </pic:spPr>
                                        </pic:pic>
                                    </a:graphicData>
                                </a:graphic>
                            </wp:inline>
                        </w:drawing>
                    </w:r>
                </w:p>
                <w:p w:rsidR="001D3714" w:rsidRPr="00EB5D1A" w:rsidRDefault="001D3714" w:rsidP="001D3714">
                    <w:pPr>
                        <w:jc w:val="center"/>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:sz w:val="20"/>
                            <w:szCs w:val="20"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                            <w:sz w:val="20"/>
                            <w:szCs w:val="20"/>
                        </w:rPr>
                        <w:t>Рис. 5.2.1. Диаграмма потоков данных</w:t>
                    </w:r>
                </w:p>
 */


/* Таблица
<w:p w:rsidR="00442D3C" w:rsidRPr="00E20E8A" w:rsidRDefault="00442D3C" w:rsidP="00442D3C">
            <w:pPr>
                <w:spacing w:after="0"/>
                <w:ind w:firstLine="709"/>
                <w:jc w:val="right"/>
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:eastAsia="ru-RU"/>
                </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00E20E8A">
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:eastAsia="ru-RU"/>
                </w:rPr>
                <w:t>Таблица 1.</w:t>
            </w:r>
            <w:r w:rsidR="00EA416A" w:rsidRPr="00E20E8A">
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:eastAsia="ru-RU"/>
                </w:rPr>
                <w:t>3</w:t>
            </w:r>
            <w:r w:rsidRPr="00E20E8A">
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:eastAsia="ru-RU"/>
                </w:rPr>
                <w:t>.1.</w:t>
            </w:r>
        </w:p>
        <w:p w:rsidR="00442D3C" w:rsidRPr="00E20E8A" w:rsidRDefault="00442D3C" w:rsidP="00442D3C">
            <w:pPr>
                <w:spacing w:after="0"/>
                <w:ind w:firstLine="709"/>
                <w:jc w:val="center"/>
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:eastAsia="ru-RU"/>
                </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00E20E8A">
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:eastAsia="ru-RU"/>
                </w:rPr>
                <w:t xml:space="preserve">Сферы и способы применения связки </w:t>
            </w:r>
            <w:r w:rsidRPr="00E20E8A">
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:val="en-US" w:eastAsia="ru-RU"/>
                </w:rPr>
                <w:t>IoT</w:t>
            </w:r>
            <w:r w:rsidRPr="00E20E8A">
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:eastAsia="ru-RU"/>
                </w:rPr>
                <w:t xml:space="preserve"> и </w:t>
            </w:r>
            <w:r w:rsidRPr="00E20E8A">
                <w:rPr>
                    <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                    <w:sz w:val="28"/>
                    <w:szCs w:val="28"/>
                    <w:lang w:val="en-US" w:eastAsia="ru-RU"/>
                </w:rPr>
                <w:t>ML</w:t>
            </w:r>
        </w:p>
        <w:tbl>
            <w:tblPr>
                <w:tblW w:w="10632" w:type="dxa"/>
                <w:tblInd w:w="-743" w:type="dxa"/>
                <w:tblBorders>
                    <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                    <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                </w:tblBorders>
                <w:tblLayout w:type="fixed"/>
                <w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="1702"/>
                <w:gridCol w:w="2410"/>
                <w:gridCol w:w="1559"/>
                <w:gridCol w:w="1843"/>
                <w:gridCol w:w="1134"/>
                <w:gridCol w:w="1984"/>
            </w:tblGrid>
            <w:tr w:rsidR="009863AB" w:rsidRPr="00E20E8A" w:rsidTr="00890DE4">
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1702" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="005F16BF" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Сфера</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2410" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="005F16BF" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Применение</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1559" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="005F16BF" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Используемые устройства</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1843" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="005F16BF" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Аналитика</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1134" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="005F16BF" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Интерфейс</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1984" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="005F16BF" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Результат</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="009863AB" w:rsidRPr="00E20E8A" w:rsidTr="00890DE4">
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1702" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00D84EE2" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Коммунальные услуги</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2410" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="002175BA" w:rsidRPr="00E20E8A" w:rsidRDefault="00D84EE2" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t xml:space="preserve">Сбор данных в режиме реального времени, </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002175BA" w:rsidRPr="00E20E8A" w:rsidRDefault="002175BA" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="002175BA" w:rsidRPr="00E20E8A" w:rsidRDefault="00D84EE2" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t xml:space="preserve">прогноз спроса-предложения, </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002175BA" w:rsidRPr="00E20E8A" w:rsidRDefault="002175BA" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00D84EE2" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>динамическое обновление тарифного плана</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1559" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00D84EE2" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t xml:space="preserve">Счетчики водо- и энергопотребления </w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1843" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="002175BA" w:rsidRPr="00E20E8A" w:rsidRDefault="009863AB" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t xml:space="preserve">Анализ показателей за прошлые коммунальные платежи, </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002175BA" w:rsidRPr="00E20E8A" w:rsidRDefault="002175BA" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="009863AB" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>предсказание спроса-предложения</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1134" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="002175BA" w:rsidRPr="00E20E8A" w:rsidRDefault="009863AB" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t xml:space="preserve">Веб-приложение, </w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="002175BA" w:rsidRPr="00E20E8A" w:rsidRDefault="002175BA" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="009863AB" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>мобильное приложение</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1984" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="009863AB" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Пользователи видят потребление и цену за коммунальные услуги, а также рекомендации по более экономному их использованию</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="009863AB" w:rsidRPr="00E20E8A" w:rsidTr="00890DE4">
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1702" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00D84EE2" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Производство</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2410" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="004B36D7" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Автоматизация конвейера</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="004B36D7" w:rsidRPr="00E20E8A" w:rsidRDefault="004B36D7" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="004B36D7" w:rsidRPr="00E20E8A" w:rsidRDefault="004B36D7" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Удаленная диагностика кейсов производства и их провалов</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1559" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="004B36D7" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Камеры</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="004B36D7" w:rsidRPr="00E20E8A" w:rsidRDefault="004B36D7" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="004B36D7" w:rsidRPr="00E20E8A" w:rsidRDefault="004B36D7" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Программируемые контроллеры</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1843" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="004D7731" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Отслеживание аномалий в использовании оборудования</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="004D7731" w:rsidRPr="00E20E8A" w:rsidRDefault="004D7731" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="004D7731" w:rsidRPr="00E20E8A" w:rsidRDefault="004D7731" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Составление плана обслуживания оборудования</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1134" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="004D7731" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Терминалы, центральные устройства управления</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1984" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="004D7731" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Сокращение затрат на ремонт и обслуживание оборудования</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="004D7731" w:rsidRPr="00E20E8A" w:rsidRDefault="004D7731" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="004D7731" w:rsidRPr="00E20E8A" w:rsidRDefault="004D7731" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>оптимизация работы конвейера</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="009863AB" w:rsidRPr="00E20E8A" w:rsidTr="00890DE4">
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1702" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00D84EE2" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Здравоохранение</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2410" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00CB4708" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Мониторинг состояния больных</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00CB4708" w:rsidRPr="00E20E8A" w:rsidRDefault="00CB4708" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00CB4708" w:rsidRPr="00E20E8A" w:rsidRDefault="00CB4708" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Диагностика</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00CB4708" w:rsidRPr="00E20E8A" w:rsidRDefault="00CB4708" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00CB4708" w:rsidRPr="00E20E8A" w:rsidRDefault="00CB4708" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Составление программ лечения</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="004D4CDC" w:rsidRPr="00E20E8A" w:rsidRDefault="004D4CDC" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="004D4CDC" w:rsidRPr="00E20E8A" w:rsidRDefault="004D4CDC" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Ведение историй болезней</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1559" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="000F4ECD" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>Медицинские приборы</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="000F4ECD" w:rsidRPr="00E20E8A" w:rsidRDefault="000F4ECD" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="000F4ECD" w:rsidRPr="00E20E8A" w:rsidRDefault="00834806" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Мобильные устройства</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1843" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00834806" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Поиск аномалий в состоянии больных (в соответствии с их медицинскими картами)</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00834806" w:rsidRPr="00E20E8A" w:rsidRDefault="00834806" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00834806" w:rsidRPr="00E20E8A" w:rsidRDefault="00834806" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Анализ данных в историях болезней</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1134" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="004F76F6" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>Медицинские мобильные терминалы</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1984" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="0013663F" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Снижение стоимости лечения</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="0013663F" w:rsidRPr="00E20E8A" w:rsidRDefault="0013663F" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="0013663F" w:rsidRPr="00E20E8A" w:rsidRDefault="0012211C" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Упрощение ведения историй болезней</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="0012211C" w:rsidRPr="00E20E8A" w:rsidRDefault="0012211C" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="0012211C" w:rsidRPr="00E20E8A" w:rsidRDefault="0012211C" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Диагностика в реальном времени</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <w:tr w:rsidR="009863AB" w:rsidRPr="00E20E8A" w:rsidTr="00890DE4">
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1702" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00D84EE2" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:lastRenderedPageBreak/>
                            <w:t>перевозки</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2410" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="00506C83" w:rsidRPr="00E20E8A" w:rsidRDefault="003E0832" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Отслеживание мест</w:t>
                        </w:r>
                        <w:r w:rsidR="00506C83" w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>оположения транспортных сре</w:t>
                        </w:r>
                        <w:proofErr w:type="gramStart"/>
                        <w:r w:rsidR="00506C83" w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t xml:space="preserve">дств </w:t>
                        </w:r>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>в р</w:t>
                        </w:r>
                        <w:proofErr w:type="gramEnd"/>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>еальном времени</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00506C83" w:rsidRPr="00E20E8A" w:rsidRDefault="00506C83" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00506C83" w:rsidRPr="00E20E8A" w:rsidRDefault="00506C83" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Отслеживание загруженности путей сообщения</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1559" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="00A25441" w:rsidRPr="00E20E8A" w:rsidRDefault="00A25441" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:val="en-US" w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>GPS-</w:t>
                        </w:r>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>передатчики, установленные на транспорте</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00A25441" w:rsidRPr="00E20E8A" w:rsidRDefault="00A25441" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00A25441" w:rsidRPr="00E20E8A" w:rsidRDefault="00A25441" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:val="en-US" w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>GPS-</w:t>
                        </w:r>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>приемники</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1843" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00342D34" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Поиск наилучшего маршрута</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00342D34" w:rsidRPr="00E20E8A" w:rsidRDefault="00342D34" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00342D34" w:rsidRPr="00E20E8A" w:rsidRDefault="00342D34" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Оптимизация логистики</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1134" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="002B4039" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Мобильные и веб приложения</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="1984" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                    </w:tcPr>
                    <w:p w:rsidR="005F16BF" w:rsidRPr="00E20E8A" w:rsidRDefault="00342D34" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Улучшение работы сервисов перевозок</w:t>
                        </w:r>
                    </w:p>
                    <w:p w:rsidR="00342D34" w:rsidRPr="00E20E8A" w:rsidRDefault="00342D34" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w:rsidR="00342D34" w:rsidRPr="00E20E8A" w:rsidRDefault="00342D34" w:rsidP="00890DE4">
                        <w:pPr>
                            <w:spacing w:after="0"/>
                            <w:jc w:val="both"/>
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E20E8A">
                            <w:rPr>
                                <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                                <w:lang w:eastAsia="ru-RU"/>
                            </w:rPr>
                            <w:t>Разгрузка путей сообщения</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
        </w:tbl>
 */