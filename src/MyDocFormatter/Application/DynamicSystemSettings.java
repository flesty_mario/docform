package MyDocFormatter.Application;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by crystall on 23.12.2017.
 */
public class DynamicSystemSettings {

    public static String databaseHost;
    public static String databasePort;
    public static String databaseName;
    public static String databaseDriver;
    public static String user;
    public static String password;

    public static String defaultSavePath;

    public static int width;
    public static int height;

    public static final String fileExtention = ".df";
    public static final String filterFileExtention = "df";
    public static final String docFileExtention = ".docx";
    public static final String tempCatalog = "./temp";
    public static final String driverMySQL = "com.mysql.jdbc.Driver";

    public static String configurationFile = "./src/MyDocFormatter/config.xml";
    public static String templateTZDirectory = "./src/MyDocFormatter/template/AB0000101TZ";
    public static String templatePZDirectory = "./src/MyDocFormatter/template/AB0000101PZ";
    public static String templateRPDirectory = "./src/MyDocFormatter/template/AB0000101RP";

    public static void loadSettings(){
        String methodName = "Loading Application Settings";
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            File file = new File(DynamicSystemSettings.configurationFile);
            FileInputStream fileInputStream = new FileInputStream(file);
            org.w3c.dom.Document config = builder.parse(fileInputStream);
            config.getDocumentElement().normalize();

            NodeList settings = config.getFirstChild().getChildNodes();
            for (int _i = 0; _i < settings.getLength(); _i++){
                //System.out.println(settings.item(_i).getNodeName() + ":: " + settings.item(_i).getNodeValue());
                if (settings.item(_i).getNodeName().equals("databaseHost")) databaseHost = settings.item(_i).getFirstChild().getNodeValue();
                if (settings.item(_i).getNodeName().equals("databasePort")) databasePort = settings.item(_i).getFirstChild().getNodeValue();
                if (settings.item(_i).getNodeName().equals("databaseName")) databaseName = settings.item(_i).getFirstChild().getNodeValue();
                if (settings.item(_i).getNodeName().equals("databaseDriver")) databaseDriver = settings.item(_i).getFirstChild().getNodeValue();
                if (settings.item(_i).getNodeName().equals("user")) user = settings.item(_i).getFirstChild().getNodeValue();
                if (settings.item(_i).getNodeName().equals("password")) password = settings.item(_i).getFirstChild().getNodeValue();
                if (settings.item(_i).getNodeName().equals("width")) width = Integer.parseInt(settings.item(_i).getFirstChild().getNodeValue());
                if (settings.item(_i).getNodeName().equals("height")) height = Integer.parseInt(settings.item(_i).getFirstChild().getNodeValue());
                if (settings.item(_i).getNodeName().equals("path")) defaultSavePath = settings.item(_i).getFirstChild().getNodeValue();
            }

            fileInputStream.close();
        }
        catch (ParserConfigurationException ex) {
            System.out.println(methodName + " XML parser configuration exception: ");
            ex.printStackTrace();
        }
        catch (SAXException ex){
            System.out.println(methodName + " SAX exception: ");
            ex.printStackTrace();
        }
        catch (IOException ex){
            System.out.println(methodName + " input/output exception: ");
            ex.printStackTrace();
        }
        catch (Exception ex){
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
        }
    }

    public static void saveSettings() {
        String methodName = "Saving Application Settings";
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            File file = new File(DynamicSystemSettings.configurationFile);
            FileInputStream fileInputStream = new FileInputStream(file);
            org.w3c.dom.Document config = builder.parse(fileInputStream);
            config.getDocumentElement().normalize();
            NodeList settings = config.getFirstChild().getChildNodes();
            for (int _i = 0; _i < settings.getLength(); _i++) {
                if (settings.item(_i).getNodeName().equals("databaseHost"))
                    settings.item(_i).getFirstChild().setNodeValue(databaseHost);
                if (settings.item(_i).getNodeName().equals("databasePort"))
                    settings.item(_i).getFirstChild().setNodeValue(databasePort);
                if (settings.item(_i).getNodeName().equals("databaseName"))
                    settings.item(_i).getFirstChild().setNodeValue(databaseName);
                if (settings.item(_i).getNodeName().equals("databaseDriver"))
                    settings.item(_i).getFirstChild().setNodeValue(databaseDriver);
                if (settings.item(_i).getNodeName().equals("user"))
                    settings.item(_i).getFirstChild().setNodeValue(user);
                if (settings.item(_i).getNodeName().equals("password"))
                    settings.item(_i).getFirstChild().setNodeValue(password);
                if (settings.item(_i).getNodeName().equals("width"))
                    settings.item(_i).getFirstChild().setNodeValue(width + "");
                if (settings.item(_i).getNodeName().equals("height"))
                    settings.item(_i).getFirstChild().setNodeValue(height + "");
                if (settings.item(_i).getNodeName().equals("path"))
                    settings.item(_i).getFirstChild().setNodeValue(defaultSavePath);
            }
            fileInputStream.close();
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(DynamicSystemSettings.configurationFile));
            Source input = new DOMSource(config);
            transformer.transform(input, output);

        } catch (ParserConfigurationException ex) {
            System.out.println(methodName + " XML parser configuration exception: ");
            ex.printStackTrace();
        } catch (SAXException ex) {
            System.out.println(methodName + " SAX exception: ");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println(methodName + " input/output exception: ");
            ex.printStackTrace();
        } catch (Exception ex) {
            System.out.println(methodName + " exception: ");
            ex.printStackTrace();
        }
    }
}
