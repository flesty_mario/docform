package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;
import MyDocFormatter.Database.JDBC;

import javax.swing.*;
import java.awt.*;

/**
 * Created by crystall on 10.04.2018.
 */
public class MainMenu extends InternalFrame{
    private JPanel rootPanel;
    private JPanel header;
    private JPanel main;

    private JDesktopPane recentPane = new JDesktopPane();
    private static RecentItems recentItems;

    private int WIDTH = (int) (DynamicSystemSettings.width * 0.93);
    private int HEIGHT = (int) (DynamicSystemSettings.height * 0.875);

    public MainMenu(JDBC databaseRequestor, Application app){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().add(header, "North");
        getContentPane().add(main, "Center");
        setSize(WIDTH, HEIGHT);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setVisible(true);

        setBorder(null);
        setBounds(new Rectangle(WIDTH, HEIGHT));
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        setLocation(0, 0);

        //editPane = new JDesktopPane();
        if (databaseRequestor != null) {
            recentItems = new RecentItems(databaseRequestor, app);
            recentPane.add(recentItems, new Integer(0));
            main.add(recentPane, "Center");
            main.repaint();
            //getContentPane().add(Editing, "Center");
            getContentPane().repaint();
        }
        else{
            JOptionPane.showMessageDialog(null, "Не удалось установить соединение с БД. Проверьте настройки");
        }
    }

    public static void refreshRecentItems(){
        recentItems.refreshRecentItems();
    }
}
