package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;
import MyDocFormatter.Database.DocumentInstance;
import MyDocFormatter.Database.JDBC;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;

/**
 * Created by crystall on 25.03.2018.
 */
public class RecentItems extends InternalFrame {
    private JPanel rootPanel;
    private JPanel mainPanel;
    private JPanel header;
    private JScrollPane scrollPane;

    //private JDesktopPane itemsList;
    private Application application;

    private JDBC db;

    private ArrayList<DocumentInstance> recentItemList;

    private int WIDTH = (int) (DynamicSystemSettings.width * 0.929);
    private int HEIGHT = (int) (DynamicSystemSettings.height * 0.875);

    private int size = -1;

    public RecentItems(JDBC _db, Application app){
        application = app;
        db = _db;
        desktopPane = new JScrollableDesktopPane();

        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent evt) {
                revalidateDesktopPane();
            }
        });

        //mainPanel = new JPanel();
        //mainPanel.setSize((int)(DynamicSystemSettings.width * 0.714), HEIGHT);
        //mainPanel.setPreferredSize(new Dimension((int) (DynamicSystemSettings.width * 0.714), HEIGHT));
        //mainPanel.setMaximumSize(new Dimension((int) (DynamicSystemSettings.width * 0.5), HEIGHT));
        //mainPanel.add(new JScrollPane(desktopPane));

        //rootPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        //getContentPane().add(new JScrollPane(desktopPane),"Center");
        //getContentPane().add(header, "North");
        //getContentPane().add(leftPanel, "West");
        //rootPanel.add(mainPanel, "Center");
        //rootPanel.add(header, "North");
        //setPreferredSize(new Dimension(WIDTH, HEIGHT));
        showRecentItems();
        desktopPane.setPreferredSize(new Dimension((int) (DynamicSystemSettings.width * 0.714), 100));
        desktopPane.setBackground(new Color(232,232,232));
        scrollPane = new JScrollPane(desktopPane);
        scrollPane.setBackground(new Color(232,232,232));

        getContentPane().add(scrollPane, "Center");

        //setContentPane(rootPanel);

        setSize(WIDTH, HEIGHT);
        setVisible(true);

        setBorder(null);
        setBackground(new Color(232,232,232));
        setBounds(new Rectangle(WIDTH, HEIGHT));
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        setLocation(0, 0);
    }

    private void showRecentItems(){
        recentItemList = db.readInstances();
        if (size == -1) size = recentItemList.size() - 1;
        for (int _i = 0; _i <  recentItemList.size(); _i++){
            DocumentInstance documentInstance = recentItemList.get(_i);

            desktopPane.add(
                    new DocumentInfo(
                            application,
                            _i,
                            size,
                            documentInstance.name,
                            documentInstance.id,
                            documentInstance.type,
                            documentInstance.createDateTime,
                            documentInstance.updateDateTime,
                            db.getLastComment(documentInstance.id),
                            documentInstance.savePath
                    ),
                    new Integer(_i)
            );
        }
        desktopPane.repaint();
    }

    public void refreshRecentItems(){
        desktopPane.removeAll();
        showRecentItems();
    }
}
