package MyDocFormatter.UI;

import MyDocFormatter.Database.DocumentInstance;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by crystall on 29.03.2018.
 */
public class SaveComment extends JDialog{
    private JPanel rootPanel;
    private JLabel lbl_docNew;
    private JTextArea txt_comment;
    private JButton saveButton;

    private int WIDTH = 550;
    private int HEIGTH = 250;

    private int result;
    private String comment;

    public SaveComment(String documentName){

        setSize(WIDTH, HEIGTH);
        setModal(true);

        result = -1;

        lbl_docNew.setText(documentName);

        ActionListener btnSave = new onClickBtnSave();
        saveButton.addActionListener(btnSave);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        getContentPane().add(rootPanel);
        getContentPane().repaint();
    }

    private void onCancel() {
        result = -1;
        dispose();
    }

    public int showDialog(){
        setVisible(true);
        return result;
    }

    private class onClickBtnSave implements ActionListener{
        public void actionPerformed(ActionEvent e){
            result = 1;
            comment = txt_comment.getText();
            dispose();
        }
    }

    public int getResult(){
        return result;
    }

    public String getComment() { return comment; }
}
