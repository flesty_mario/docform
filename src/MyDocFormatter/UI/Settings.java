package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * Created by crystall on 07.04.2018.
 */
public class Settings extends InternalFrame {
    private JPanel rootPanel;
    private JPanel dbPanel;
    private JPanel commonPanel;
    private JTextField txt_driver;
    private JTextField txt_host;
    private JTextField txt_user;
    private JTextField txt_port;
    private JTextField txt_dbname;
    private JButton btn_cancel;
    private JButton btn_apply;
    private JTextField txt_width;
    private JPanel footer;
    private JTextField txt_height;
    private JTextField txt_pwd;
    private JTextField txt_path;
    private JButton btn_path;

    private int WIDTH = (int) ((double)DynamicSystemSettings.width * 0.7);
    private int HEIGHT = (int) ((double)DynamicSystemSettings.height * 0.75);

    public Settings(){
        setSize(WIDTH, HEIGHT);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setVisible(true);

        setBorder(null);
        setBounds(new Rectangle(WIDTH, HEIGHT));
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        setLocation(0, 0);

        txt_host.setText(DynamicSystemSettings.databaseHost);
        txt_driver.setText(DynamicSystemSettings.databaseDriver);
        txt_dbname.setText(DynamicSystemSettings.databaseName);
        txt_port.setText(DynamicSystemSettings.databasePort);
        txt_user.setText(DynamicSystemSettings.user);
        txt_pwd.setText(DynamicSystemSettings.password);
        txt_width.setText(DynamicSystemSettings.width + "");
        txt_height.setText(DynamicSystemSettings.height + "");
        txt_path.setText(DynamicSystemSettings.defaultSavePath);

        ActionListener btnCancel = new onCancel();
        btn_cancel.addActionListener(btnCancel);

        ActionListener btnApply = new onApply();
        btn_apply.addActionListener(btnApply);

        ActionListener btnSelectPath = new onSelectPath();
        btn_path.addActionListener(btnSelectPath);

        getContentPane().add(rootPanel);
        getContentPane().repaint();
    }

    private class onApply implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            DynamicSystemSettings.databaseHost = txt_host.getText();
            DynamicSystemSettings.databaseDriver = txt_driver.getText();
            DynamicSystemSettings.databaseName = txt_dbname.getText();
            DynamicSystemSettings.databasePort = txt_port.getText();
            DynamicSystemSettings.user = txt_user.getText();
            DynamicSystemSettings.password = txt_pwd.getText();
            DynamicSystemSettings.width = Integer.parseInt(txt_width.getText());
            DynamicSystemSettings.height = Integer.parseInt(txt_height.getText());
            DynamicSystemSettings.defaultSavePath = txt_path.getText();
            DynamicSystemSettings.saveSettings();
            JOptionPane.showMessageDialog(null, "Настройки сохранены. Чтобы изменения вступили в силу, нужно перезапустить программу");
        }
    }

    private class onCancel implements ActionListener {
        public void actionPerformed(ActionEvent e) {

        }
    }

    private class onSelectPath implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                JFileChooser fileChooser = new JFileChooser(DynamicSystemSettings.defaultSavePath);
                int ret = fileChooser.showDialog(null, "Применить");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    String fileFullName = fileChooser.getSelectedFile().getPath();
                    String fileName = fileChooser.getSelectedFile().getName();
                    String path = fileFullName.split(fileName)[0];
                    txt_path.setText(path);
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

}
