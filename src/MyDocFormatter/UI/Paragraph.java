package MyDocFormatter.UI;

import MyDocFormatter.utils.PrintedForms.XmlMaster;
import org.w3c.dom.Node;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by crystall on 05.03.2018.
 */
public class Paragraph extends JInternalFrame {
    private JPanel rootPanel;
    private JTextArea textParagraph;
    private Node editableNode = null; //xml-элемент из текстового документа, который редактируется данным элементом
    private Node parentNode = null;   //родительский xml-элемент
    private int paragraphIndex;       //индекс родительского элемента
    private int elementIndex;         //индекс текущего элемента в родительском
    private String paragraphText;

    public Paragraph(String text, int index, int w, int h, int offset, Node parentNode, int elementIndex){
        paragraphText = text;
        paragraphIndex = index;
        this.elementIndex = elementIndex;
        this.parentNode = parentNode;
        textParagraph.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                paragraphText = textParagraph.getText();
                if (editableNode != null) {
                    XmlMaster.setValue(WorkArea.currentEditingDocument.getParsedXml(0), editableNode, paragraphText);
                    //System.out.println("Value inserted for NODE " + paragraphIndex);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                paragraphText = textParagraph.getText();
                if (editableNode != null) {
                    XmlMaster.setValue(WorkArea.currentEditingDocument.getParsedXml(0), editableNode, paragraphText);
                    //System.out.println("Value deleted for NODE " + paragraphIndex);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                paragraphText = textParagraph.getText();
                if (editableNode != null) {
                    XmlMaster.setValue(WorkArea.currentEditingDocument.getParsedXml(0), editableNode, paragraphText);
                    //System.out.println("Value updated for NODE " + paragraphIndex);
                }
            }
        });
        textParagraph.setText(paragraphText);
        textParagraph.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                WorkArea.elementIndex = paragraphIndex;
                WorkArea.treadIndex = elementIndex;
                WorkArea.paragraphNode = parentNode;
                System.out.println("Editing paragraph: " + WorkArea.elementIndex);
                System.out.println("Editing thread: " + WorkArea.treadIndex);
                System.out.println("Paragraph: " + WorkArea.paragraphNode);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        setContentPane(rootPanel);
        setSize(w, h);
        setVisible(true);
        //setFocusable(false);
        //setBounds(0, 0, 300, 60);
        setBorder(null);
        setBounds(new Rectangle(w, h));
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        setLocation(0, offset);
    }

    public int getParagraphIndex(){ return paragraphIndex; }
    public String getParagraphText() { return paragraphText; }

    public void linkWithThread(Node documentThread){
        editableNode = documentThread;
    }
}
