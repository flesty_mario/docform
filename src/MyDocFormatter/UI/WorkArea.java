package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;
import MyDocFormatter.Database.DocumentInstance;
import MyDocFormatter.Database.JDBC;
import MyDocFormatter.utils.Common;
import MyDocFormatter.utils.PrintedForms.Document;
import MyDocFormatter.utils.PrintedForms.PrintedFormsSettings;
import org.w3c.dom.Node;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.Style;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by crystall on 17.03.2018.
 */
public class WorkArea extends InternalFrame{
    private JPanel rootPanel;
    private JPanel Actions;
    private JPanel Editing;
    private JButton btn_new;
    private JButton btn_open;
    private JButton btn_save;
    private JButton btn_delete;
    private JButton btn_history;
    private JPanel toolbar;
    private JPanel docPanel;
    private JButton btn_addTable;
    private JPanel buttons;
    private JButton btn_textBefore;
    private JButton btn_textAfter;
    private JButton btn_paragraphAfter;
    private JButton btn_paragraphBefore;
    private JButton btn_deleteText;
    private JButton btn_deleteParagraph;
    private JButton btn_deleteTable;
    private JCheckBox cb_bold;
    private JCheckBox cb_italic;
    private JCheckBox cb_underline;
    private JRadioButton rb_both;
    private JRadioButton rb_left;
    private JRadioButton rb_center;
    private JRadioButton rb_right;
    private JComboBox cb_font;
    private JComboBox cb_size;
    private JButton btn_insertList;
    private JLabel lbl_name;
    private JLabel txt_ver;
    private JButton btn_deleteAll;
    private JButton btn_code;
    private JButton btn_h1;
    private JButton btn_h2;
    private JButton btn_h3;

    private JDesktopPane editPane = new JDesktopPane();
    private EditDocument editDocument;

    public static Document currentEditingDocument;
    private int versionId;
    private int version;

    private int scrollPosition;

    private JDBC databaseRequestor;

    public static int elementIndex = 0;
    public static int treadIndex = 0;
    public static Node paragraphNode;

    private int WIDTH = (int) (DynamicSystemSettings.width * 0.93);
    private int HEIGHT = (int) (DynamicSystemSettings.height * 0.875);

    public WorkArea(JDBC _databaseRequestor){
        databaseRequestor = _databaseRequestor;

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getContentPane().add(Actions, "West");
        getContentPane().add(Editing, "Center");
        setSize(WIDTH, HEIGHT);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setVisible(true);

        /*cb_font.addItem("Times New Roman");
        cb_font.addItem("Calibri");
        cb_font.addItem("Arial");
        cb_font.addItem("Courier New");

        cb_size.addItem("8.5");
        cb_size.addItem("9");
        cb_size.addItem("9.5");
        cb_size.addItem("10");
        cb_size.addItem("10.5");
        cb_size.addItem("11");
        cb_size.addItem("11.5");
        cb_size.addItem("12");
        cb_size.addItem("12.5");
        cb_size.addItem("13");
        cb_size.addItem("13.5");
        cb_size.addItem("14");
        cb_size.addItem("14.5");
        cb_size.addItem("15");
        cb_size.addItem("15.5");
        cb_size.addItem("16");
        cb_size.addItem("16.5");
        cb_size.addItem("17");*/

        ActionListener btnNewDoc = new onClickNew();
        btn_new.addActionListener(btnNewDoc);

        ActionListener btnSaveDoc = new onClickSaveDoc();
        btn_save.addActionListener(btnSaveDoc);

        ActionListener btnOpenDoc = new onClickOpenDoc();
        btn_open.addActionListener(btnOpenDoc);

        ActionListener btnOpenHistory = new onClickOpenHistory();
        btn_history.addActionListener(btnOpenHistory);

        ActionListener btnDeleteVersion = new onClickDeleteVersion();
        btn_delete.addActionListener(btnDeleteVersion);

        ActionListener btnDeleteAll = new onClickDeleteAll();
        btn_deleteAll.addActionListener(btnDeleteAll);

        ActionListener btnAddTable = new onAddTableClick();
        btn_addTable.addActionListener(btnAddTable);

        ActionListener btnAddParagraphBefore = new onAddParagraphBefore();
        btn_paragraphBefore.addActionListener(btnAddParagraphBefore);

        ActionListener btnAddParagraphAfter = new onAddParagraphAfter();
        btn_paragraphAfter.addActionListener(btnAddParagraphAfter);

        ActionListener btnAddThreadBefore = new onAddThreadBefore();
        btn_textBefore.addActionListener(btnAddThreadBefore);

        ActionListener btnAddThreadAfter = new onAddThreadAfter();
        btn_textAfter.addActionListener(btnAddThreadAfter);

        ActionListener btnDeleteThread = new onDeleteThread();
        btn_deleteText.addActionListener(btnDeleteThread);

        ActionListener btnDeleteParagraph = new onDeleteParagraph();
        btn_deleteParagraph.addActionListener(btnDeleteParagraph);

        ActionListener btnDeleteTable = new onDeleteTable();
        btn_deleteTable.addActionListener(btnDeleteTable);

        ActionListener btnInsertList = new onInsertList();
        btn_insertList.addActionListener(btnInsertList);

        ActionListener btnInsertCode = new onInsertCode();
        btn_code.addActionListener(btnInsertCode);

        ActionListener btnInsertH1 = new onInsertHeaderLevel1();
        btn_h1.addActionListener(btnInsertH1);

        ActionListener btnInsertH2 = new onInsertHeaderLevel2();
        btn_h2.addActionListener(btnInsertH2);

        ActionListener btnInsertH3 = new onInsertHeaderLevel3();
        btn_h3.addActionListener(btnInsertH3);

        setBorder(null);
        setBounds(new Rectangle(WIDTH, HEIGHT));
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        setLocation(0, 0);
    }

    public void createNewWorkArea(Document _currentEditingDocument){
        int scrollPos = 0;
        /*if (editDocument != null) {
            scrollPos = editDocument.getScrollPosition();
        }*/
        currentEditingDocument = _currentEditingDocument;
        lbl_name.setText(currentEditingDocument.getName());
        //getContentPane().remove(Editing);
        //Editing = new JPanel();
        Editing.remove(editPane);
        editPane = new JDesktopPane();
        editDocument = new EditDocument(currentEditingDocument.getDocumentContent());
        editPane.add(editDocument, new Integer(0));
        Editing.add(editPane, "Center");
        Editing.repaint();
        //getContentPane().add(Editing, "Center");
        getContentPane().repaint();
        //editDocument.setFocus(treadIndex);
        //editDocument.setScrollPosition(scrollPos);
    }

    public void refreshWorkArea(){
        //getContentPane().remove(Editing);
        //Editing = new JPanel();
        //int scrollPos = 0;
        //int scrollPosX = 0;
        Rectangle viewRect = new Rectangle(0,0);
        if (editDocument != null) {
            scrollPosition = editDocument.getScrollPosition();
            //viewRect = editDocument.getScrollVisibleRect();
            if (currentEditingDocument != null) {
                editDocument.rebuildUI(currentEditingDocument.getDocumentContent());
                //Editing.add(editPane, "Center");
                //editDocument.setScrollVisibleRect(viewRect);
                //editPane.scrollRectToVisible(viewRect);
                editDocument.setScrollPosition(scrollPosition);
            }
        }
        else {
            editPane = new JDesktopPane();
            editDocument = new EditDocument(currentEditingDocument.getDocumentContent());
            editPane.add(editDocument, new Integer(0));
            Editing.add(editPane, "Center");
        }
        //Editing.remove(editPane);

        //Editing.repaint();
        //getContentPane().revalidate();
        //editDocument.setScrollPosition(scrollPos);
        //getContentPane().add(Editing, "Center");
        //getContentPane().repaint();
    }

    public void repaintWorkArea(){
        getContentPane().repaint();
    }

    public void openDocument(DocumentInstance instance){
        versionId = databaseRequestor.readInstance(instance);
        if (versionId == -2){
            JOptionPane.showMessageDialog(null, "Текущая версия была сохранена с ошибкой. Будет открыта предыдущая версия");
            openDocument(instance);
        }
        else {
            version = databaseRequestor.getVersionNumber(versionId);
            setCurrentEditingDocument(instance);
            //createNewWorkArea(currentEditingDocument);
        }
    }

    private void setCurrentEditingDocument(DocumentInstance instance){
        ArrayList<String> temporaryXmlList = Common.StringArrayToArrayList(PrintedFormsSettings.temporaryXmlFiles[instance.type]);
        ArrayList<String> documentXmlList = Common.StringArrayToArrayList(PrintedFormsSettings.documentEntries[instance.type]);
        currentEditingDocument = new Document(instance.name, instance.savePath, DynamicSystemSettings.tempCatalog + "/" + instance.name, documentXmlList, temporaryXmlList, instance.type);
        currentEditingDocument.setId(instance.id);
        currentEditingDocument.setUpdateDateTime(instance.updateDateTime);
        //System.out.println("FULL PATH: " + instance.savePath + "" + instance.name);
        lbl_name.setText(currentEditingDocument.getName());
        txt_ver.setText(version + "");
        refreshWorkArea();
    }

    private class onClickNew implements ActionListener{
        public void actionPerformed(ActionEvent e){
            GenerateDocument generateDocument = new GenerateDocument();
            generateDocument.pack();
            if (generateDocument.showDialog() == 1){
                createNewWorkArea(generateDocument.getDocument());
            }
        }
    }

    private class onClickSaveDoc implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String comment = "";
            SaveComment saveComment = new SaveComment(currentEditingDocument.getName());
            saveComment.pack();
            if (saveComment.showDialog() != -1){
                comment = saveComment.getComment();
                currentEditingDocument.saveDocument();
                DocumentInstance instance = currentEditingDocument.createInstance();
                instance.commentary = comment;
                databaseRequestor.saveInstance(instance);
                currentEditingDocument.setId(instance.id);
                currentEditingDocument.createDocumentParametersFile();
                versionId = databaseRequestor.getLastVersionId(instance.id);
                version = databaseRequestor.getVersionNumber(versionId);
                //System.out.println("versionId: " + versionId + " version: " + version);
                txt_ver.setText(version + "");
                Application.recentItems.refreshRecentItems();
                //getContentPane().repaint();
            }
        }
    }

    private class onClickDeleteVersion implements ActionListener{
        public void actionPerformed(ActionEvent e){
            int dialogResult = JOptionPane.showConfirmDialog (null, "Вы уверены, что хотите удалить текущую версию?","Удаление версии", JOptionPane.YES_NO_OPTION);
            if(dialogResult == JOptionPane.YES_OPTION){
                databaseRequestor.removeVersion(versionId);
                openDocument(currentEditingDocument.createInstance());
                refreshWorkArea();
                Application.recentItems.refreshRecentItems();
            }
        }
    }

    private class onClickDeleteAll implements ActionListener{
        public void actionPerformed(ActionEvent e){
            try {
                int dialogResult = JOptionPane.showConfirmDialog (null, "Вы уверены, что хотите удалить файл? При удалении файла будет также удалена и вся история изменений.","Удаление файла", JOptionPane.YES_NO_OPTION);
                if(dialogResult == JOptionPane.YES_OPTION){
                    DocumentInstance documentInstance = currentEditingDocument.createInstance();
                    databaseRequestor.removeInstance(documentInstance);
                    System.out.println("FP: " + documentInstance.savePath + documentInstance.name);
                    if (!currentEditingDocument.deleteSettingsFile()){
                        JOptionPane.showMessageDialog(null, "Файл не был найден в " + documentInstance.savePath + ". Возможно, он был перемещен или удален.");
                    }
                    currentEditingDocument = null;
                    txt_ver.setText("");
                    lbl_name.setText("");
                    elementIndex = -1;
                    treadIndex = -1;
                    paragraphNode = null;
                    refreshWorkArea();
                    Application.recentItems.refreshRecentItems();
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private class onClickOpenDoc implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //currentEditingDocument.saveDocument();
            try {
                String documentName = "";
                int type = 0;
                String updateDateTime = "";
                int id = 0;
                System.out.println(new File(DynamicSystemSettings.defaultSavePath));
                JFileChooser fileChooser = new JFileChooser(DynamicSystemSettings.defaultSavePath);
                FileFilter fileChooserFilter = new FileNameExtensionFilter("MyDocFormatter files", DynamicSystemSettings.filterFileExtention);
                fileChooser.setFileFilter(fileChooserFilter);
                int ret = fileChooser.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {

                    FileReader fileReader = new FileReader(fileChooser.getSelectedFile());
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    documentName = bufferedReader.readLine();
                    type = Integer.parseInt(bufferedReader.readLine());
                    updateDateTime = bufferedReader.readLine();
                    id = Integer.parseInt(bufferedReader.readLine());
                    bufferedReader.close();
                    fileReader.close();

                    DocumentInstance instance = new DocumentInstance();
                    instance.name = documentName;
                    instance.type = type;
                    instance.updateDateTime = updateDateTime;
                    instance.id = id;

                    openDocument(instance);
                    currentEditingDocument.setPath(fileChooser.getSelectedFile().getPath());
                }
            }
            catch (FileNotFoundException ex){
                ex.printStackTrace();
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private class onClickOpenHistory implements ActionListener{
        public void actionPerformed(ActionEvent e){
            try {
                DocumentHistory documentHistory = new DocumentHistory(databaseRequestor, currentEditingDocument.getId());
                documentHistory.pack();
                int _versionId = -1;
                if (documentHistory.showDialog() != -1){
                    _versionId = documentHistory.getVersionId();
                }
                //System.out.println("Versin id: " + versionId);
                if (_versionId != -1) {
                    versionId = _versionId;
                    DocumentInstance documentInstance = databaseRequestor.getVersion(versionId);
                    version = databaseRequestor.getVersionNumber(versionId);
                    setCurrentEditingDocument(documentInstance);
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private class onAddTableClick implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                AddTable addTable = new AddTable();
                addTable.pack();
                if (addTable.showDialog() == 1) {
                    if (currentEditingDocument != null) {
                        currentEditingDocument.insertTable(
                                addTable.getColumnsWidth(),
                                addTable.getMapValue(),
                                addTable.getTableName(),
                                addTable.getTablePurpose(),
                                WorkArea.elementIndex
                        );
                    }
                    refreshWorkArea();
                }
            }
        }
    }

    private class onAddParagraphBefore implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                String fontSize = "28";
                String align = "both";
                String font = "Times New Roman";
                String spacingBefore = "360";
                String spacingAfter = "240";
                currentEditingDocument.insertParagraph("", font, fontSize, align, spacingBefore, spacingAfter, false, false, false, elementIndex, true);
                refreshWorkArea();
            }
        }
    }

    private class onAddParagraphAfter implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                String fontSize = "28";
                String align = "both";
                String font = "Times New Roman";
                String spacingBefore = "360";
                String spacingAfter = "240";
                currentEditingDocument.insertParagraph("", font, fontSize, align, spacingBefore, spacingAfter, false, false, false, elementIndex, false);
                refreshWorkArea();
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onAddThreadBefore implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                String fontSize = "28";
                String font = "Times New Roman";
                currentEditingDocument.insertThread("", font, fontSize, false, false, false, paragraphNode, treadIndex, true);
                refreshWorkArea();
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onAddThreadAfter implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                String fontSize = "28";
                String font = "Times New Roman";
                currentEditingDocument.insertThread("", font, fontSize, false, false, false, paragraphNode, treadIndex, false);
                refreshWorkArea();
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onDeleteThread implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                currentEditingDocument.deleteThread(elementIndex, treadIndex);
                refreshWorkArea();
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onDeleteParagraph implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                currentEditingDocument.deleteParagraph(elementIndex);
                refreshWorkArea();
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onDeleteTable implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                currentEditingDocument.deleteTable(elementIndex);
                refreshWorkArea();
            }
           //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onInsertList implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                AddList addList = new AddList();
                addList.pack();
                if (addList.showDialog() == 1) {
                        currentEditingDocument.insertList(
                                addList.getValuesList(),
                                WorkArea.elementIndex,
                                false,
                                false,
                                false
                        );
                    refreshWorkArea();
                }
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onInsertCode implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                String fontSize = "17";
                String align = "both";
                String font = "Courier New";
                String spacingBefore = "";
                String spacingAfter = "";
                currentEditingDocument.insertParagraph("", font, fontSize, align, spacingBefore, spacingAfter, false, false, false, elementIndex, true);
                refreshWorkArea();
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onInsertHeaderLevel1 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                String fontSize = "36";
                String align = "center";
                String font = "Times New Roman";
                String spacingBefore = "120";
                String spacingAfter = "120";
                currentEditingDocument.insertParagraph("", font, fontSize, align, spacingBefore, spacingAfter, true, false, false, elementIndex, true);
                refreshWorkArea();
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onInsertHeaderLevel2 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                String fontSize = "28";
                String align = "center";
                String font = "Times New Roman";
                String spacingBefore = "360";
                String spacingAfter = "240";
                currentEditingDocument.insertParagraph("", font, fontSize, align, spacingBefore, spacingAfter, true, false, false, elementIndex, true);
                refreshWorkArea();
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }

    private class onInsertHeaderLevel3 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (currentEditingDocument != null) {
                String fontSize = "28";
                String align = "left";
                String font = "Times New Roman";
                String spacingBefore = "360";
                String spacingAfter = "240";
                currentEditingDocument.insertParagraph("", font, fontSize, align, spacingBefore, spacingAfter, false, true, false, elementIndex, true);
                refreshWorkArea();
            }
            //editDocument.setScrollPosition(scrollPosition);
        }
    }
}
