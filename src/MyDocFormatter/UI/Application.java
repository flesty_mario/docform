package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;
import MyDocFormatter.Database.JDBC;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by crystall on 25.03.2018.
 */
public class Application extends JFrame {
    private JPanel rootPanel;
    private JTabbedPane tabbedPane1;
    private JPanel historyPanel;
    private JPanel mainPanel;
    private JPanel settingsPanel;
    private JPanel helpPanel;

    //private JDesktopPane recentItems;
    //private JDesktopPane editing;
    //private JDesktopPane settings;

    private int WIDTH;
    private int HEIGHT;

    public static WorkArea workArea;
    public static MainMenu recentItems;
    private JDBC databaseClient;                              //клиент для работы с БД

    public Application(){
        DynamicSystemSettings.loadSettings();

        String connection = "jdbc:" + DynamicSystemSettings.databaseDriver +
                "://" + DynamicSystemSettings.databaseHost +
                ":" + DynamicSystemSettings.databasePort +
                "/" + DynamicSystemSettings.databaseName;
        //System.out.println(connection);
        WIDTH = DynamicSystemSettings.width;
        HEIGHT = DynamicSystemSettings.height;

        try {
            databaseClient = new JDBC(connection, DynamicSystemSettings.user, DynamicSystemSettings.password);
        }
        catch (Exception ex){
            databaseClient = null;
        }

        workArea = new WorkArea(databaseClient);
        recentItems = new MainMenu(databaseClient, this);

        //recentItems = new JDesktopPane();
        //editing = new JDesktopPane();
        //settings = new JDesktopPane();
        //settings.add(new Settings(), new Integer(0));
        historyPanel = new JPanel();
        historyPanel.add(recentItems, new Integer(0));
        mainPanel = new JPanel();
        mainPanel.add(workArea, new Integer(0));
        settingsPanel = new JPanel();
        settingsPanel.add(new Settings(), new Integer(0));
        helpPanel = new JPanel();
        helpPanel.add(new Help(), new Integer(0));

        tabbedPane1.addTab("Главная", null, historyPanel);
        tabbedPane1.addTab("Редактирование", null, mainPanel);
        tabbedPane1.addTab("Настройки", null, settingsPanel);
        tabbedPane1.addTab("Помощь", null, helpPanel);

        tabbedPane1.repaint();

        getContentPane().add(tabbedPane1, "Center");
        getContentPane().repaint();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        setVisible(true);
    }

    public void changeTab(int tabIndex){
        tabbedPane1.setSelectedIndex(tabIndex);
    }
}
