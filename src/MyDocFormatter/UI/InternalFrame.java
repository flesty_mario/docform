package MyDocFormatter.UI;

import javax.swing.*;
import java.awt.*;

/**
 * Created by crystall on 25.03.2018.
 */
public abstract class InternalFrame  extends JInternalFrame {

    protected JScrollableDesktopPane desktopPane;

    protected void revalidateDesktopPane() {
        Dimension dim = new Dimension(0,0);
        Component[] com = desktopPane.getComponents();
        for (int i=0 ; i<com.length ; i++) {
            int w = (int) dim.getWidth()+com[i].getWidth();
            int h = (int) dim.getHeight()+com[i].getHeight();
            dim.setSize(new Dimension(w,h));
        }
        desktopPane.setPreferredSize(dim);
        desktopPane.revalidate();
        revalidate();
        repaint();
    }
}
