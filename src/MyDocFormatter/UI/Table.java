package MyDocFormatter.UI;

import MyDocFormatter.utils.PrintedForms.XmlMaster;
import org.w3c.dom.Node;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by crystall on 08.03.2018.
 */
public class Table extends JInternalFrame {
    private JPanel rootPanel;
    private JTable table;
    private DefaultTableModel tableModel;
    private int tableIndex;

    private Node[][] threadMatrix;

    public Table(int index, int rowNum, int colNum, int offset){
        tableIndex = index;
        int height = rowNum * 10;

        setContentPane(rootPanel);
        setSize(900, height);
        setVisible(true);

        JFrame jfrm = new JFrame("table");
        jfrm.getContentPane().setLayout(new FlowLayout());
        jfrm.setSize(900, height);
        JScrollPane jscrlp = new JScrollPane(table);

        tableModel = new DefaultTableModel();
        for (int _i = 0; _i < colNum; _i++){
            tableModel.addColumn("");
        }

        threadMatrix = new Node[rowNum][colNum];

        for (int _j = 0; _j < rowNum; _j++){
            Vector row = new Vector();
            for (int _i = 0; _i < colNum; _i++){
                row.add("");
            }
            tableModel.addRow(row);
            tableModel.newRowsAdded(new TableModelEvent(tableModel));
        }
        tableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                int row = e.getFirstRow();
                int col = e.getColumn();
                if (row != -1 && col != -1) {
                    changeCellValue(row, col, String.valueOf(table.getValueAt(row, col)));
                }
            }
        });

        table.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                WorkArea.elementIndex = tableIndex;
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        table.setModel(tableModel);
        rootPanel.add(jscrlp);
        rootPanel.updateUI();
        setBorder(null);
        setBounds(new Rectangle(900, height));
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        setLocation(0, offset);
    }

    public void setCellValue(int row, int col, String value){
        tableModel.setValueAt(value, row, col);
        tableModel.newDataAvailable(new TableModelEvent(tableModel));
    }

    public void linkCellWithThread(int row, int col, Node thread){
        threadMatrix[row][col] = thread;
    }

    private void changeCellValue(int row, int col, String value){
        XmlMaster.setValue(WorkArea.currentEditingDocument.getParsedXml(0),threadMatrix[row][col], value);
        //System.out.println("Changed value in table " + tableIndex + " at cell: " + row + " " + col);
    }
}
