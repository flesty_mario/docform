package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;
import MyDocFormatter.utils.PrintedForms.Unit;
import MyDocFormatter.utils.PrintedForms.XmlMaster;
import org.w3c.dom.Node;

import javax.accessibility.AccessibleContext;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by crystall on 04.03.2018.
 */
public class EditDocument extends InternalFrame {
    private JPanel rootPanel;
    private JScrollPane scrollPane;
    //private JButton btn_addTable;
    private ArrayList<Node> documentContentList = new ArrayList<>();
    private ArrayList<JInternalFrame> workAreaUI = new ArrayList<>();

    private int WIDTH = (int) (DynamicSystemSettings.width * 0.643);
    private int HEIGHT = (int) (DynamicSystemSettings.height * 0.875);

    private int scrollPosition;

    //private WorkArea workArea;

    public EditDocument(ArrayList<Node> content){

        //this.workArea = workArea;
        //textArea1.sel
        desktopPane = new JScrollableDesktopPane();


        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent evt) {
                revalidateDesktopPane();
            }
        });

        //ActionListener btnAddTable = new onAddTableClick();
        //btn_addTable.addActionListener(btnAddTable);

        buildUI(content);
        //desktopPane.repaint();
        //desktopPane.setPreferredSize(new Dimension(WIDTH, 100));
        scrollPane = new JScrollPane(desktopPane);
        getContentPane().add(scrollPane, "Center");
        getContentPane().repaint();

        setSize(WIDTH, HEIGHT);
        setVisible(true);

        setBorder(null);
        setBounds(new Rectangle(WIDTH, HEIGHT));
        //((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        setLocation(150, 10);
    }

    public void rebuildUI(ArrayList<Node> content){
        documentContentList.clear();
        desktopPane.removeAll();
        workAreaUI.clear();

        buildUI(content);
        getContentPane().repaint();
    }

    private void buildUI(ArrayList<Node> content){
        int offset = 0;
        int layout = 0;

        for (int index = 0; index < content.size(); index++){
            Node node = content.get(index);
            if (XmlMaster.isTable(node)){
                Table table = new Table(index, XmlMaster.getTableRows(node).size(), XmlMaster.getMaxColsNum(node), offset);
                ArrayList<Unit> rows = XmlMaster.getTableRows(node);
                for (int _i = 0; _i < rows.size(); _i++){
                    ArrayList<Unit> rowContentList = XmlMaster.getRowContent(rows.get(_i).element);
                    for (Unit u: rowContentList){
                        documentContentList.add(u.element);
                    }
                    for (int _j = 0; _j < XmlMaster.getRowContentSize(rows.get(_i).element); _j++){
                        table.linkCellWithThread(_i, _j, XmlMaster.getRowContent(rows.get(_i).element).get(_j).element);
                        table.setCellValue(_i, _j, XmlMaster.getText(XmlMaster.getRowContent(rows.get(_i).element).get(_j).element));
                        layout++;
                    }
                }
                desktopPane.add(table, new Integer(index));
                offset += (XmlMaster.getTableRows(node).size()*10);
                workAreaUI.add(table);
            }
            else if (XmlMaster.isParagraph(node)){
                //System.out.println(node.toString() + " is paragraph ");
                ArrayList<Unit> threadList = XmlMaster.getThreadList(node);
                for (int _i = 0; _i < threadList.size(); _i++) {
                    String value = XmlMaster.getText(threadList.get(_i).element);
                    Paragraph paragraph = new Paragraph(value, index, WIDTH, (value.length() / WIDTH + 1)*20, offset, node, threadList.get(_i).index);
                    paragraph.linkWithThread(threadList.get(_i).element);
                    desktopPane.add(paragraph, new Integer(index));
                    for (Unit u: threadList){
                        documentContentList.add(u.element);
                    }
                    //documentContentList.addAll(XmlMaster.getThreadList(node));
                    offset += (value.length() / WIDTH + 1)*20;
                    workAreaUI.add(paragraph);
                }
            }
        }
    }

    public void setFocus(){
        workAreaUI.get(WorkArea.elementIndex + WorkArea.treadIndex).grabFocus();
    }

    public void setScrollPosition(int position){
        scrollPosition = position;
        getContentPane().repaint();
        System.out.println("old scroll pos: " + position);
        System.out.println("current scroll position: " + scrollPane.getVerticalScrollBar().getValue());
        scrollPane.getVerticalScrollBar().setValue(position);
        scrollPane.getHorizontalScrollBar().setValue(0);
        System.out.println("Scroll position: " + scrollPane.getVerticalScrollBar().getValue());

        //scrollRectToVisible(new Rectangle(500,400,200,200) );
        //updateUI();
        //scrollPane.repaint();
    }

    public void setScrollVisibleRect(Rectangle viewRect){
        System.out.println("RECT TO: " + viewRect);
        desktopPane.computeVisibleRect(viewRect);
        desktopPane.scrollRectToVisible(viewRect);
        System.out.println("RECT AFTER: " + desktopPane.getVisibleRect());
    }

    public int getScrollPosition(){
        return scrollPane.getVerticalScrollBar().getValue();
    }

    public Rectangle getScrollVisibleRect(){
        return desktopPane.getVisibleRect();
    }

    /*private class Cb1Listener implements ItemListener {
        public void itemStateChanged(ItemEvent e) {
            scrollPane.getVerticalScrollBar().setValue(scrollPosition);
        }
    }*/

}
