package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;

import javax.swing.*;
import java.awt.*;

/**
 * Created by SKhaustov on 12.04.2018.
 */
public class Help extends InternalFrame{
    private JPanel rootPanel;
    private JTabbedPane tabbedPane1;
    private JTextPane common;
    private JTextPane creating;
    private JTextPane editing;
    private JTextPane history;
    private JTextPane settings;

    private int WIDTH = DynamicSystemSettings.width;
    private int HEIGHT = DynamicSystemSettings.height;

    public Help(){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setVisible(true);
        setBorder(null);
        setContentPane(rootPanel);
        setBounds(new Rectangle(WIDTH, HEIGHT));
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        setLocation(0, 0);
    }

}
