package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;
import MyDocFormatter.Database.DocumentInstance;
import MyDocFormatter.Database.JDBC;
import MyDocFormatter.utils.PrintedForms.PrintedFormsSettings;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by crystall on 25.03.2018.
 */
public class DocumentHistory extends JDialog {
    private JPanel rootPanel;
    private JPanel bottomPanel;
    private JPanel mainPanel;
    private JButton btn_open;
    private JTable table_history;

    private DefaultTableModel tableModel;

    private JDBC databaseRequestor;
    private ArrayList<DocumentInstance> historyList;
    private int refId;
    private int versionId = -1;

    private int WIDTH = (int) (DynamicSystemSettings.width * 0.571);
    private int HEIGTH = (int) (DynamicSystemSettings.height * 0.375);

    public DocumentHistory(JDBC databaseRequestor, int refId){
        this.databaseRequestor = databaseRequestor;
        this.refId = refId;

        setSize(WIDTH, HEIGTH);
        setModal(true);

        ActionListener btnOpenDoc = new onClickBtnOpenDoc();
        btn_open.addActionListener(btnOpenDoc);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        JFrame jfrm = new JFrame("History");
        jfrm.getContentPane().setLayout(new FlowLayout());
        jfrm.setSize(WIDTH, HEIGTH);
        JScrollPane jscrlp = new JScrollPane(table_history);

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tableModel.addColumn("Версия");
        tableModel.addColumn("Дата создания");
        tableModel.addColumn("Комментарий");

        table_history.setModel(tableModel);
        table_history.getColumn("Версия").setWidth(80);
        table_history.getColumn("Версия").setMaxWidth(80);
        table_history.getColumn("Версия").setMinWidth(80);
        table_history.getColumn("Дата создания").setWidth(120);
        table_history.getColumn("Дата создания").setMaxWidth(120);
        table_history.getColumn("Дата создания").setMinWidth(120);
        table_history.getColumn("Комментарий").setWidth(400);
        table_history.getColumn("Комментарий").setMaxWidth(400);
        table_history.getColumn("Комментарий").setMinWidth(400);
        //table_history.setEnabled(false);

        loadHistory();

        getContentPane().add(jscrlp);
        getContentPane().add(bottomPanel, "South");
        getContentPane().repaint();

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void loadHistory(){
        historyList = databaseRequestor.readHistory(refId);
        for (DocumentInstance instance: historyList) {
            Vector newRow = new Vector();
            newRow.add(instance.version);
            newRow.add(instance.updateDateTime);
            newRow.add(instance.commentary);
            tableModel.addRow(newRow);
            tableModel.newRowsAdded(new TableModelEvent(tableModel));
        }
    }

    private void onCancel() {
        versionId = -1;
        dispose();
    }

    private class onClickBtnOpenDoc implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if (table_history.getSelectedRow() != -1) {
                versionId = historyList.get(table_history.getSelectedRow()).id;
                dispose();
            }
        }
    }

    public int showDialog(){
        setVisible(true);
        return versionId;
    }

    public int getVersionId(){
        return versionId;
    }

}
