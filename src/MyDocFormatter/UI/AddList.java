package MyDocFormatter.UI;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by crystall on 06.04.2018.
 */
public class AddList extends JDialog {
    private JTable table_list;
    private JPanel rootPanel;
    private JButton btn_minus;
    private JButton btn_plus;
    private JButton ОКButton;
    private JPanel header;
    private JPanel footer;
    private JPanel Center;

    private DefaultTableModel tableModel;
    private int result;
    private ArrayList<String> valuesList;

    private int WIDTH = 700;
    private int HEIGTH = 450;

    public AddList(){
        tableModel = new DefaultTableModel();
        tableModel.addColumn("");

        valuesList = new ArrayList<>();

        setSize(WIDTH, HEIGTH);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setModal(true);

        result = -1;

        JFrame jfrm = new JFrame("table");
        jfrm.getContentPane().setLayout(new FlowLayout());
        jfrm.setSize(WIDTH, HEIGTH-70);
        JScrollPane jscrlp = new JScrollPane(table_list);

        table_list.setModel(tableModel);

        Center = new JPanel();

        Center.add(jscrlp);
        Center.updateUI();

        ActionListener btnOk = new onOk();
        ОКButton.addActionListener(btnOk);

        ActionListener btnAddRowClickListener = new onClickBtnAddRow();
        btn_plus.addActionListener(btnAddRowClickListener);

        ActionListener btnDeleteRowClickListener = new onClickBtnDeleteRow();
        btn_minus.addActionListener(btnDeleteRowClickListener);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        rootPanel.add(header, "North");
        rootPanel.add(Center, "Center");
        rootPanel.add(footer, "South");

        getContentPane().add(rootPanel);
        getContentPane().repaint();
    }

    private void onCancel() {
        result = -1;
        dispose();
    }

    private class onOk implements ActionListener {
        public void actionPerformed(ActionEvent e){
            for (int _i = 0; _i < table_list.getRowCount(); _i++){
                valuesList.add(table_list.getValueAt(_i,0).toString());
            }
            result = 1;
            dispose();
        }
    }

    private class onClickBtnAddRow implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if (table_list.getSelectedRow() == -1 || table_list.getSelectedRow() == table_list.getRowCount()-1) {
                tableModel.addRow(new Vector());
                //valuesList.add("");
            }
            else {
                tableModel.insertRow(table_list.getSelectedRow()+1, new Vector());
                //valuesList.add(table_list.getSelectedRow()+1,"");
            }
            tableModel.newRowsAdded(new TableModelEvent(tableModel));
            //System.out.println(commandList.size());
        }
    }

    private class onClickBtnDeleteRow implements ActionListener{
        public void actionPerformed(ActionEvent e){
            int row = table_list.getSelectedRow();
            if (row != -1) {
                //valuesList.remove(row);
                tableModel.removeRow(row);
            }
        }
    }

    public int showDialog(){
        setVisible(true);
        return result;
    }

    public ArrayList<String> getValuesList(){
        return valuesList;
    }
}
