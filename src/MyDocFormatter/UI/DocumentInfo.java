package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;
import MyDocFormatter.Database.DocumentInstance;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by crystall on 25.03.2018.
 */
public class DocumentInfo extends JInternalFrame {
    private JPanel rootPanel;
    private JButton btn_opendoc;
    private JLabel lbl_name;
    private JLabel lbl_update_date;
    private JLabel lbl_comment;

    private int id;
    private String name;
    private String path;
    private int type;
    private String createDateTime;
    private String updateDateTime;

    private Application application;

    private int WIDTH = (int)(DynamicSystemSettings.width * 0.5);
    private int HEIGHT = (int)(DynamicSystemSettings.height * 0.175);

    public DocumentInfo(Application application, int order, int offsetX, String _name, int _id, int _type, String _createDateTime, String _updateDateTime, String _comment, String savePath){
        this.application = application;
        id = _id;
        name = _name;
        path = savePath;
        type = _type;
        createDateTime = _createDateTime;
        updateDateTime = _updateDateTime;

        int offset = (HEIGHT*order);
        //System.out.println("offset: " + offset);
        lbl_name.setText(name);
        lbl_update_date.setText(updateDateTime);
        lbl_comment.setText(_comment);

        ActionListener btnOpenDoc = new DocumentInfo.onClicBtnOpen();
        btn_opendoc.addActionListener(btnOpenDoc);

        setContentPane(rootPanel);
        setSize(WIDTH, HEIGHT);
        setVisible(true);
        setBorder(null);
        setBounds(new Rectangle(WIDTH, HEIGHT));
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);
        setLocation((int) (WIDTH*0.5), offset);
    }

    private class onClicBtnOpen implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            DocumentInstance instance = new DocumentInstance();
            instance.name = name;
            instance.type = type;
            instance.updateDateTime = updateDateTime;
            instance.id = id;
            instance.savePath = path;

            application.changeTab(1);
            application.workArea.openDocument(instance);
        }
    }
}
