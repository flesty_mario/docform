package MyDocFormatter.UI;

import MyDocFormatter.Application.DynamicSystemSettings;
import MyDocFormatter.utils.Common;
import MyDocFormatter.utils.PrintedForms.Document;
import MyDocFormatter.utils.PrintedForms.PrintedFormsSettings;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by crystall on 03.03.2018.
 */
public class GenerateDocument extends JDialog {
    private JPanel rootPanel;
    private JPanel footer;
    private JButton b_create;
    private JList pattern_list;
    private JPanel Header;
    private JTable table_properties;
    private JTextField txt_name;
    private JPanel Prp;
    private JPanel CommonProp;
    private DefaultTableModel tableModel;

    private int patternKey;
    private Document document;

    private int result = -1;

    private int WIDTH = (int) (DynamicSystemSettings.width * 0.928);
    private int HEIGHT = (int) (DynamicSystemSettings.height * 0.75);

    public GenerateDocument(){
        setSize(WIDTH, HEIGHT);
        setModal(true);

        pattern_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                patternKey = pattern_list.getSelectedIndex();
                setPropertyInputForm(patternKey);
            }
        });

        ActionListener btnGenerateDoc = new onClickb_create();
        b_create.addActionListener(btnGenerateDoc);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        JFrame jfrm = new JFrame("Creator");
        jfrm.getContentPane().setLayout(new FlowLayout());
        jfrm.setSize((int) (DynamicSystemSettings.width * 0.714), HEIGHT);
        JScrollPane jscrlp = new JScrollPane(table_properties);

        tableModel = new DefaultTableModel();
        tableModel.addColumn("Поле");
        tableModel.addColumn("Значение");
        table_properties.setModel(tableModel);
        table_properties.getColumn("Поле").setWidth(500);
        table_properties.getColumn("Поле").setMaxWidth(500);
        table_properties.getColumn("Поле").setMinWidth(500);


        Prp = new JPanel();
        Prp.setLayout(new BorderLayout());
        //Prp.setPreferredSize(new Dimension((int) (DynamicSystemSettings.width * 0.714), (int) (DynamicSystemSettings.height * 0.625)));
        //Prp.setMinimumSize(new Dimension((int) (DynamicSystemSettings.width * 0.714), (int) (DynamicSystemSettings.height * 0.625)));
        //Prp.setMaximumSize(new Dimension((int) (DynamicSystemSettings.width * 0.714), (int) (DynamicSystemSettings.height * 0.625)));
        Prp.add(CommonProp,"West");
        Prp.add(jscrlp, "Center");
        rootPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        rootPanel.add(Header, "North");
        rootPanel.add(Prp, "Center");
        rootPanel.add(footer, "South");
        setContentPane(rootPanel);
        rootPanel.updateUI();

    }

    private void setPropertyInputForm(int patternIndex){
        tableModel.getDataVector().removeAllElements();
        tableModel.rowsRemoved(new TableModelEvent(tableModel));

        /*for (String property: PrintedFormsSettings.templateProperties[patternIndex]){
            Vector newRow = new Vector();
            newRow.add(property);
            newRow.add("");
            tableModel.addRow(newRow);
            tableModel.newRowsAdded(new TableModelEvent(tableModel));
        }*/

        for (int _i = 0; _i < PrintedFormsSettings.templateProperties[patternIndex].length; _i++){
            Vector newRow = new Vector();
            newRow.add(PrintedFormsSettings.templateProperties[patternIndex][_i]);
            newRow.add(PrintedFormsSettings.testData[patternIndex][_i]);
            tableModel.addRow(newRow);
            tableModel.newRowsAdded(new TableModelEvent(tableModel));
        }
    }

    private void onCancel() {
        result = -1;
        dispose();
    }

    public int showDialog(){
        setVisible(true);
        return result;
    }

    public Document getDocument(){
        return document;
    }

    private class onClickb_create implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String name = txt_name.getText();
            if ("".equals(name)){
                JOptionPane.showMessageDialog(null, "Сначала нужно ввести название документа");
                return;
            }
            ArrayList<String> templateXmlList = Common.StringArrayToArrayList(PrintedFormsSettings.templateXmlFiles[patternKey]);
            ArrayList<String> temporaryXmlList = Common.StringArrayToArrayList(PrintedFormsSettings.temporaryXmlFiles[patternKey]);
            ArrayList<String> documentXmlList = Common.StringArrayToArrayList(PrintedFormsSettings.documentEntries[patternKey]);
            document = new Document(
                    name + DynamicSystemSettings.docFileExtention,
                    DynamicSystemSettings.defaultSavePath,
                    PrintedFormsSettings.documentTemplateName[patternKey],
                    templateXmlList,
                    documentXmlList,
                    temporaryXmlList,
                    patternKey
            );
            Map<String, String> values = new HashMap<String, String>();
            //JTextArea editValue = new JTextArea();
            int _j = 0;
            for (int _i = 0; _i < tableModel.getRowCount(); _i++){
                String type = PrintedFormsSettings.templatePropertiesTypes[patternKey][_j];
                String value = tableModel.getValueAt(_i, 1).toString();
                if (type.contains("DAY")){
                    String[] date = value.trim().split(" ");
                    for (String part: date){
                        values.put(type, part);
                        _j++;
                        type = PrintedFormsSettings.templatePropertiesTypes[patternKey][_j];
                        //System.out.println("CREATING DOC: Put value " + part + " at label " + type);
                    }
                }
                else {
                    values.put(type, value);
                    //System.out.println("CREATING DOC: Put value " + value + " at label " + type);
                    _j++;
                }
            }

            for (int _i = 0; _i < templateXmlList.size(); _i++)
                document.setNodesValues(values, _i);
            document.generateDocument();
            result = 1;
            dispose();
        }
    }
}
