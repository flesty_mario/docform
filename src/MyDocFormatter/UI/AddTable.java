package MyDocFormatter.UI;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by crystall on 31.03.2018.
 */
public class AddTable extends JDialog {
    private JPanel rootPanel;
    private JTextField txt_rows;
    private JTextField txt_cols;
    private JPanel settings;
    private JPanel footer;
    private JButton btn_ok;
    private JTable table;
    private JPanel tablePanel;
    private JTextField table_name;
    private JTextField table_purpose;
    private DefaultTableModel tableModel;

    private int result;

    private int WIDTH = 700;
    private int HEIGTH = 450;

    private int rowsCount;
    private int columnsCount;

    private ArrayList<String> columnsWidth;
    private ArrayList<ArrayList<String>> mapValue;
    private String tableName;
    private String tablePurpose;

    public AddTable(){
        tableModel = new DefaultTableModel();

        setSize(WIDTH, HEIGTH);
        setModal(true);

        result = -1;

        ActionListener btnOk = new onOk();
        btn_ok.addActionListener(btnOk);

        JFrame jfrm = new JFrame("table");
        jfrm.getContentPane().setLayout(new FlowLayout());
        jfrm.setSize(WIDTH, HEIGTH);
        JScrollPane jscrlp = new JScrollPane(table);

        txt_rows.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                resizeTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {

            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }
        });
        txt_cols.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                resizeTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {

            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        table.setModel(tableModel);

        tablePanel = new JPanel();

        tablePanel.add(jscrlp);
        tablePanel.updateUI();
        rootPanel.add(settings, "North");
        rootPanel.add(tablePanel, "Center");
        rootPanel.add(footer, "South");

        getContentPane().add(rootPanel);
        getContentPane().repaint();
    }

    private void resizeTable(){
        try {
            rowsCount = Integer.parseInt(txt_rows.getText().trim());
            columnsCount = Integer.parseInt(txt_cols.getText().trim());
        }
        catch (Exception ex){
            rowsCount = -1;
            columnsCount = -1;
        }
        if (rowsCount != -1 && columnsCount != -1) {
            createTableModel(rowsCount, columnsCount);
        }
    }

    private void createTableModel(int rowNum, int colNum){
        tableModel = new DefaultTableModel();
        for (int _i = 0; _i < colNum; _i++){
            tableModel.addColumn("");
        }
        for (int _j = 0; _j < rowNum; _j++){
            Vector row = new Vector();
            for (int _i = 0; _i < colNum; _i++){
                row.add("");
            }
            tableModel.addRow(row);
            tableModel.newRowsAdded(new TableModelEvent(tableModel));
        }
        table.setModel(tableModel);
        //System.out.println("Rows: " + rowsCount + " Columns: " + columnsCount);
        tablePanel.repaint();
    }

    private void onCancel() {
        result = -1;
        dispose();
    }

    private class onOk implements ActionListener{
        public void actionPerformed(ActionEvent e){
            columnsWidth = new ArrayList<>();
            mapValue = new ArrayList<>();
            for (int _i = 0; _i < columnsCount; _i++){
                columnsWidth.add(""+table.getColumnModel().getColumn(_i).getWidth());
            }
            for (int _i = 0; _i < rowsCount; _i++){
                ArrayList<String> row = new ArrayList<>();
                for (int _j = 0; _j < columnsCount; _j++){
                    row.add(table.getValueAt(_i, _j).toString());
                }
                mapValue.add(row);
            }
            tableName = table_name.getText();
            tablePurpose = table_purpose.getText();
            result = 1;
            dispose();
        }
    }

    public int showDialog(){
        setVisible(true);
        return result;
    }

    public ArrayList<String> getColumnsWidth(){ return columnsWidth; }

    public ArrayList<ArrayList<String>> getMapValue() { return mapValue; }

    public String getTableName() { return tableName; }

    public String getTablePurpose() { return tablePurpose; }
}
