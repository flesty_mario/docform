package MyDocFormatter.Database;

/**
 * Created by crystall on 24.03.2018.
 */
public class DocumentInstance {
    public String name;
    public int type;
    public String createDateTime;
    public String updateDateTime;
    public int id;
    public int version;
    public String commentary;
    public String savePath;
}
