package MyDocFormatter.Database;

import MyDocFormatter.Application.DynamicSystemSettings;

import java.awt.*;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by crystall on 18.03.2018.
 */
public class JDBC {
    private String url;
    private String username;
    private String password;

    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;
    private PreparedStatement preparedStatement;

    private String DOCUMENT_TABLE = "DOCUMENT";
    private String HISTORY_DOCUMENT_TABLE = "HISTORY_DOCUMENT";
    private String BLOB_COLUMN = "FILESTREAM";

    private String DRIVER = DynamicSystemSettings.driverMySQL;

    public JDBC(String _url, String _username, String _password) throws SQLException, ClassNotFoundException {
        try {
            url = _url;
            username = _username;
            password = _password;
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(url, username, password);
            System.out.println("connection successful!");
        }
        catch (SQLException ex){
            throw new SQLException(ex.getMessage());
        }
        catch (ClassNotFoundException ex){
            throw new ClassNotFoundException(ex.getMessage());
        }
    }

    public int getInstanceId(DocumentInstance instance){
        int id = -1;
        try {
            String query = "SELECT ID FROM document WHERE NAME = '" + instance.name + "' AND Type = '" + instance.type + "'";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt("ID");
            }
            statement.close();
            resultSet.close();
            return id;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return id;
        }
    }

    public DocumentInstance getVersion(int versionId){
        try{
            String query = "SELECT REFID, FILESTREAM FROM history_document WHERE ID = '" + versionId + "'";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            int id = -1;
            Blob blobFile = connection.createBlob();
            while (resultSet.next()) {
                id = resultSet.getInt("REFID");
                blobFile = resultSet.getBlob("FILESTREAM");
            }
            statement.close();
            resultSet.close();

            query = "SELECT * FROM document WHERE ID = '" + id + "'";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            DocumentInstance instance = new DocumentInstance();
            while (resultSet.next()) {
                instance.id = resultSet.getInt("ID");
                instance.name = resultSet.getString("NAME");
                instance.updateDateTime = resultSet.getString("UPDATEDATETIME");
                instance.createDateTime = resultSet.getString("CREATEDATETIME");
                instance.savePath = resultSet.getString("SAVEPATH");
                instance.type = resultSet.getInt("Type");
            }
            statement.close();
            resultSet.close();

            File f = new File(DynamicSystemSettings.tempCatalog + "/" + instance.name);
            FileOutputStream fileOutputStream = new FileOutputStream(f);
            fileOutputStream.write(blobFile.getBytes(1, (int) blobFile.length()));
            fileOutputStream.close();
            return instance;

        }
        catch (SQLException ex){
            ex.printStackTrace();
            return null;
        }
        catch (FileNotFoundException ex){
            ex.printStackTrace();
            return null;
        }
        catch (IOException ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void saveInstance(DocumentInstance instance){
        try {
            //новый документ
            if (instance.id == -1){
                preparedStatement = connection.prepareStatement(
                        "INSERT INTO document (NAME, CREATEDATETIME, UPDATEDATETIME, SAVEPATH, Type) values (?, ?, ?, ?, ?);"
                );
                preparedStatement.setString(1, instance.name);
                preparedStatement.setString(2, instance.createDateTime);
                preparedStatement.setString(3, instance.updateDateTime);
                preparedStatement.setString(4, instance.savePath);
                preparedStatement.setInt(5, instance.type);
                preparedStatement.execute();
                System.out.println("insert new document successful!");
                preparedStatement.close();
                instance.id = getInstanceId(instance);
            }

            Blob blobFile = connection.createBlob();
            File f = new File(instance.savePath + "" + instance.name);
            FileInputStream fis = new FileInputStream(f);
            byte[] blobContent = new byte[(int)f.length()];
            fis.read(blobContent);
            blobFile.setBytes(1, blobContent);
            preparedStatement = connection.prepareStatement(
                    "INSERT INTO history_document (REFID, VERSION, FILESTREAM, CREATEDATETIME, COMMENT) values (?, ?, ?, ?, ?);"
            );
            preparedStatement.setInt(1, instance.id);
            preparedStatement.setInt(2, createVersionId(instance.id));
            preparedStatement.setBlob(3, blobFile);
            preparedStatement.setString(4, instance.updateDateTime);
            preparedStatement.setString(5, instance.commentary);
            preparedStatement.execute();
            preparedStatement = connection.prepareStatement(
                    "UPDATE document SET UPDATEDATETIME = ? WHERE id = ?;"
            );
            preparedStatement.setString(1, instance.updateDateTime);
            preparedStatement.setInt(2, instance.id);
            preparedStatement.execute();
            System.out.println("insert successful!");
            preparedStatement.close();
            fis.close();
        }
        catch (FileNotFoundException ex){
            ex.printStackTrace();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public ArrayList<DocumentInstance> readInstances(){
        try{
            ArrayList<DocumentInstance> documentInstances = new ArrayList<>();
            String query = "SELECT * FROM document ORDER BY UpdateDatetime DESC";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                documentInstances.add(new DocumentInstance());
                int index = documentInstances.size() - 1;
                documentInstances.get(index).id = resultSet.getInt("ID");
                documentInstances.get(index).name = resultSet.getString("NAME");
                documentInstances.get(index).type = resultSet.getInt("type");
                documentInstances.get(index).updateDateTime = resultSet.getString("UpdateDatetime");
                documentInstances.get(index).createDateTime = resultSet.getString("createDatetime");
                documentInstances.get(index).savePath = resultSet.getString("SAVEPATH");
            }
            statement.close();
            resultSet.close();
            System.out.println("Readed successful " + documentInstances.size() + " instances");
            return documentInstances;
        }
        catch (Exception ex){
            System.out.println("Error reading instances");
            return null;
        }
    }

    public ArrayList<DocumentInstance> readHistory(int id){
        try{
            ArrayList<DocumentInstance> documentHistory = new ArrayList<>();
            String query = "SELECT * FROM history_document WHERE REFID = '" + id + "'";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                documentHistory.add(new DocumentInstance());
                int index = documentHistory.size() - 1;
                documentHistory.get(index).id = resultSet.getInt("ID");
                documentHistory.get(index).version = resultSet.getInt("version");
                documentHistory.get(index).commentary = resultSet.getString("COMMENT");
                documentHistory.get(index).updateDateTime = resultSet.getString("CREATEDATETIME");
            }
            statement.close();
            resultSet.close();
            System.out.println("Readed successful " + documentHistory.size() + " histories of doc: " + id);
            return documentHistory;
        }
        catch (Exception ex){
            System.out.println("Error reading instances");
            return null;
        }
    }

    public String getLastComment(int relId){
        try{
            String comment = "";
            String query =
                    "SELECT ID, COMMENT FROM history_document WHERE REFID = '" + relId + "'";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            int max_id = -1;
            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                if (id >= max_id) {
                    comment = resultSet.getString("COMMENT");
                    max_id = id;
                }
            }
            return comment;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return "";
        }
    }

    public int getLastVersionId(int relId){
        try{
            int versionId = -1;
            String query =
                    "SELECT ID FROM history_document WHERE REFID = '" + relId + "'";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                if (id >= versionId) {
                    versionId = id;
                }
            }
            return versionId;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return -1;
        }
    }

    public int getVersionNumber(int versionId){
        try{
            int versionNumber = 1;
            String query =
                    "SELECT version FROM history_document WHERE ID = '" + versionId + "'";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                versionNumber = resultSet.getInt("version");
            }
            System.out.println("versionNumber: " + versionNumber);
            return versionNumber;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return -1;
        }
    }

    private String getSavePath(int id){
        try {
            String path = "";
            String query =
                    "SELECT ID, SAVEPATH FROM document WHERE ID = '" + id + "'";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                path = resultSet.getString("SAVEPATH");
            }
            return path;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return "";
        }
    }

    private int createVersionId(int docID){
            try{
                String query =
                        "SELECT VERSION FROM history_document WHERE REFID = '" + docID + "'";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);
                int maxVersionId = 0;
                while (resultSet.next()) {
                    int versionId = resultSet.getInt("VERSION");
                    if (versionId >= maxVersionId) {
                        maxVersionId = versionId;
                    }
                }
                return ++maxVersionId;
            }
            catch (SQLException ex){
                ex.printStackTrace();
                return -1;
            }
    }

    public int readInstance(DocumentInstance instance){
        try {
            Blob blobFile = null;
            File f = new File(DynamicSystemSettings.tempCatalog + "/" + instance.name);
            int relId = getInstanceId(instance);
            //System.out.println("REL ID " + relId);
            if (relId != -1) {
                instance.savePath = getSavePath(relId);
                FileOutputStream fileOutputStream = new FileOutputStream(f);
                String query =
                        "SELECT * FROM history_document WHERE REFID = '" + relId + "'";
                statement = connection.createStatement();
                resultSet = statement.executeQuery(query);
                int max_id = -1;
                while (resultSet.next()) {
                    int id = resultSet.getInt("ID");
                    if (id >= max_id) {
                        blobFile = resultSet.getBlob(BLOB_COLUMN);
                        max_id = id;
                    }
                    //System.out.println("Readed id: " + max_id);
                }
                if (blobFile.length() == 0){
                    System.out.println("A blob is empty. Version " + max_id + " will be removed");
                    removeVersion(max_id);
                    return -2;
                }
                if (blobFile != null) {
                    fileOutputStream.write(blobFile.getBytes(1, (int) blobFile.length()));
                }
                fileOutputStream.close();
                statement.close();
                resultSet.close();
                return max_id;
            }
            else {
                System.out.println("Error reading instance");
                return -1;
            }
        }
        catch (FileNotFoundException ex){
            ex.printStackTrace();
            return -1;
        }
        catch (SQLException ex){
            ex.printStackTrace();
            return -1;
        }
        catch (IOException ex){
            ex.printStackTrace();
            return -1;
        }
    }

    public void removeInstance(DocumentInstance instance){
        try{
            preparedStatement = connection.prepareStatement("DELETE FROM HISTORY_DOCUMENT WHERE REFID = ?");
            preparedStatement.setInt(1, instance.id);
            preparedStatement.execute();
            preparedStatement.close();
            preparedStatement = connection.prepareStatement("DELETE FROM DOCUMENT WHERE ID = ?");
            preparedStatement.setInt(1, instance.id);
            preparedStatement.execute();
            System.out.println("delete ins successful!");
            preparedStatement.close();
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public void removeVersion(int versionId){
        try{
            preparedStatement = connection.prepareStatement("DELETE FROM HISTORY_DOCUMENT WHERE ID = ?");
            preparedStatement.setInt(1, versionId);
            preparedStatement.execute();
            System.out.println("delete version successful!");
            preparedStatement.close();
        }
        catch (SQLException ex){
            ex.printStackTrace();
        }
    }

}
